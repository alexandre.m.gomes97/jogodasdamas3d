﻿using SimpleFileBrowser;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace JogoDasDamas
{


public class PerfilEditorView : MonoBehaviour
{
    public delegate void MetodosSemParametros();
    public static event MetodosSemParametros PedidoReturn;
    public static event MetodosSemParametros PedidoSave;

    public InputField nome;
    public InputField email;
    public InputField pais;
    public Text wins;
    public Text defeats;
    public Text draws;
    public Text n_games;
    public Text a_games;
    public Text vicP;
    public Text username;
    public RawImage photo;
    
   

        string imagePath;
       

        void Start()
    {
        PerfilEditorModel.ReturnB += PerfilEditorModel_ReturnB;
        PerfilEditorModel.Save += PerfilEditorModel_Save;
            
        nome.text = GameSystem.Instance.PlayerLogged.name;
        email.text = GameSystem.Instance.PlayerLogged.email;
        pais.text = GameSystem.Instance.PlayerLogged.pais;
        wins.text = GameSystem.Instance.PlayerLogged.wins.ToString();
        defeats.text = GameSystem.Instance.PlayerLogged.defeats.ToString();
        draws.text = GameSystem.Instance.PlayerLogged.draws.ToString();
        n_games.text = GameSystem.Instance.PlayerLogged.n_games.ToString();
        a_games.text = GameSystem.Instance.PlayerLogged.a_games.ToString();
            GameSystem.Instance.PlayerLogged.WinPercentageCalc();
        vicP.text = GameSystem.Instance.PlayerLogged.WinPercentage.ToString();
        username.text = GameSystem.Instance.PlayerLogged.username;
        WWW newImage = new WWW("file:///" + GameSystem.Instance.PlayerLogged.photoPath);
        photo.texture = newImage.texture;
            GameSystem.Instance.PlayerLogged.WinPercentageCalc();
            vicP.text = GameSystem.Instance.PlayerLogged.WinPercentage.ToString() + '%';
        }

   


    private void PerfilEditorModel_Save()
    {
        MessageBox.messageInfo = "Save Completed!";
        MessageBox.titleInfo = "Save";
        SceneManager.LoadSceneAsync("MessageBox", LoadSceneMode.Additive);
    }

    private void PerfilEditorModel_ReturnB()
    {
        SceneManager.UnloadSceneAsync("PerfilEditor");
    }

    //BUTTONS START

    public void Btn_Cancel()
    {
        
        if (PedidoReturn != null)
            PedidoReturn();
    }

    public void Btn_Save()
    {
        LoginModel.Player1.username = username.text;
        LoginModel.Player1.name = nome.text;
        LoginModel.Player1.pais = pais.text;
        LoginModel.Player1.email = email.text;
        if (PedidoSave != null)
            PedidoSave();
    }

     public void Btn_Photo()
        {
            FileBrowser.SetFilters(true, new FileBrowser.Filter("Images", ".jgp", ".png"));
            FileBrowser.SetDefaultFilter(".jpg");
            FileBrowser.SetExcludedExtensions(".lnk", ".tmp", ".zip", ".rar", ".exe");
            FileBrowser.AddQuickLink("Users", "C:\\Users", null);
            StartCoroutine(ShowLoadDialogCoroutine());
        }

        IEnumerator ShowLoadDialogCoroutine()
        {
            // Show a load file dialog and wait for a response from user
            // Load file/folder: file, Initial path: default (Documents), Title: "Load File", submit button text: "Load"
            yield return FileBrowser.WaitForLoadDialog(false, null, "Load File", "Load");
            // Dialog is closed
            // Print whether a file is chosen (FileBrowser.Success)
            // and the path to the selected file (FileBrowser.Result) (null, if FileBrowser.Success is false)
            Debug.Log(FileBrowser.Success + " " + FileBrowser.Result);
            imagePath = FileBrowser.Result;
            PerfilEditorModel.player.photoPath = FileBrowser.Result;
            GetImage();
        }

        void GetImage()
        {
            if (imagePath != null)
            {
                UpdateImage();
            }
        }

        void UpdateImage()
        {
            WWW newImage = new WWW("file:///" + imagePath);
            photo.texture = newImage.texture;
        }
        //BUTTONS END

        public void OnDisable()
    {
        PerfilEditorModel.ReturnB -= PerfilEditorModel_ReturnB;
        PerfilEditorModel.Save -= PerfilEditorModel_Save;
    }
}
}