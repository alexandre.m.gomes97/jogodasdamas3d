﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JogoDasDamas
{


public class PerfilEditorController : MonoBehaviour
{
    public PerfilEditorModel PerfilEditorModel { get; set; } = new PerfilEditorModel();
    string AlterUserURL = "http://localhost/Player/AlterProfile.php";

    public void OnEnable()
    {
        PerfilEditorView.PedidoReturn += PerfilEditorView_PedidoReturn;
        PerfilEditorView.PedidoSave += PerfilEditorView_PedidoSave;
        PerfilEditorModel.AlterUser += PerfilEditorModel_AlterUser;
    }

    private void PerfilEditorModel_AlterUser(JogoDasDamas.Player player1)
    {
        WWWForm form = new WWWForm();
        form.AddField("usernamePost", player1.username);
        form.AddField("passwordPost", player1.password);
        form.AddField("nomePost", player1.name);
        form.AddField("emailPost", player1.email);
        form.AddField("paisPost", player1.pais);
        player1.photoPath = player1.photoPath.Replace("\\", "\\\\");
        form.AddField("photoPathPost", player1.photoPath);
        form.AddField("ID_PPost", player1.ID_P);
        

            WWW www = new WWW(AlterUserURL, form);
    }

    private void PerfilEditorView_PedidoSave()
    {
        
        PerfilEditorModel.onButtonSave();
    }

    private void PerfilEditorView_PedidoReturn()
    {
        PerfilEditorModel.onButtonReturn();
    }

    public void OnDisable()
    {
        PerfilEditorView.PedidoReturn -= PerfilEditorView_PedidoReturn;
        PerfilEditorView.PedidoSave -= PerfilEditorView_PedidoSave;
            PerfilEditorModel.AlterUser -= PerfilEditorModel_AlterUser;
        }

}
}
