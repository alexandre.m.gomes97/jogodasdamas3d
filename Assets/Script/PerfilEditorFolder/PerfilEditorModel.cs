﻿using JogoDasDamas;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JogoDasDamas
{
    public class PerfilEditorModel
    {
        public delegate void MetodoSemParametros();
        public static event MetodoSemParametros ReturnB;
        public static event MetodoSemParametros Save;
        public static event MetodoComUmPlayer AlterUser;

        public static Player player;
       

        public PerfilEditorModel()
        {


        }

        public void onButtonReturn()
        {

            if (ReturnB != null)
                ReturnB();
        }

        public void onButtonSave()
        {
            if (AlterUser != null)
                AlterUser(LoginModel.Player1);
            GameSystem.Instance.PlayerLogged = LoginModel.Player1;
            //verificar se existe um username igual
            if (Save != null)
                Save();
        }

    }
}

