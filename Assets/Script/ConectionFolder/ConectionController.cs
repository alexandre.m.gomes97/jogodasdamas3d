﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

namespace JogoDasDamas
{
    public class ConectionController : MonoBehaviour
    {
        public ConectionModel ConectionModel { get; set; } = new ConectionModel();

        public void OnEnable()
        {
            ConectionView.PedidoCancel += ConectionView_PedidoCancel;
            ConectionView.PedidoConnect += ConectionView_PedidoConnect;
        }

        private void ConectionView_PedidoConnect(string IP)
        {
            try
            {
                ConectionModel.OnButtonConnect(IP);
            }
            catch(Exception e)
            {
                ConectionModel.SendError(e.Message);
            }
            
        }

        private void ConectionView_PedidoCancel()
        {
            ConectionModel.OnCancelButton();
        }

        public void OnDisable()
        {
            ConectionView.PedidoCancel -= ConectionView_PedidoCancel;
            ConectionView.PedidoConnect -= ConectionView_PedidoConnect;
        }

    }
}
 



