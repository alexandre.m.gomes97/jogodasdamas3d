﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace JogoDasDamas
{
    public class ConectionView : MonoBehaviour
    {
        public static event MetodoSemParametros PedidoCancel;
        public static event MetodoComUmaString PedidoConnect;

        

        public InputField serverIp;


        void Start()
        {
            ConectionModel.Cancel += ConectionModel_Cancel;
            ConectionModel.RespostaErro += ConectionModel_RespostaErro;
        }

        private void ConectionModel_RespostaErro(string msg)
        {
            MessageBox.messageInfo = msg;
            MessageBox.titleInfo = "Error!";
            SceneManager.LoadSceneAsync("MessageBox", LoadSceneMode.Additive);
        }

        private void ConectionModel_Cancel()
        {
            SceneManager.UnloadSceneAsync("ServerConection");
        }


        //BUTTONS START

        public void ConectBtn()
        {
            string ipServer = serverIp.text;

            if (PedidoConnect != null)
                PedidoConnect(ipServer);


        }

        public void CancelBtn()
        {
            if (PedidoCancel != null)
                PedidoCancel();
        }


        //BUTTONS END

        //VIEW







        // Update is called once per frame
        void Update()
        {

        }

        public void OnDisable()
        {
            ConectionModel.Cancel -= ConectionModel_Cancel;
            ConectionModel.RespostaErro -= ConectionModel_RespostaErro;
        }

    }
}
