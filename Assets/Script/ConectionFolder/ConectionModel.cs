﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace JogoDasDamas
{
    public class ConectionModel
    {
        public delegate void MetodoSemParametros();
        public static event MetodoSemParametros Cancel;
        public static event MetodoComUmaString RespostaErro;
        public static event MetodoSemParametros RespostaConnect;

        public void OnCancelButton()
        {
            if (Cancel != null)
                Cancel();
        }

        public void OnButtonConnect(string ipServer)
        {
            var ipSplited = ipServer.Split('.');
            


            if (ipServer == "" || ipSplited.Length != 4)
            {
                throw new Exception("Connection not possible check the ip");
            }
            else
            {
                if (RespostaConnect != null)
                    RespostaConnect();
            }

        }

        public void SendError(string msg)
        {
            if (RespostaErro != null)
                RespostaErro(msg);
        }

    }
}



