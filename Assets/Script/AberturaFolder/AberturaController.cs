﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AberturaController : MonoBehaviour
{
    public AberturaModel AberturaModel {get;set;} = new AberturaModel();
    public void OnEnable()
    {
        AberturaView.PedidoLogin += AberturaView_PedidoLogin;
        AberturaView.PedidoRegisto += AberturaView_PedidoRegisto;
        AberturaView.PedidoQuit += AberturaView_PedidoQuit;
    }

    private void AberturaView_PedidoQuit()
    {
        AberturaModel.onButtonQuit();
    }

    private void AberturaView_PedidoRegisto()
    {
        AberturaModel.onButtonRegister();
    }

    private void AberturaView_PedidoLogin()
    {
        AberturaModel.onLoginBtn();
    }

    public void OnDisable()
    {
        AberturaView.PedidoLogin -= AberturaView_PedidoLogin;
        AberturaView.PedidoRegisto -= AberturaView_PedidoRegisto;
        AberturaView.PedidoQuit -= AberturaView_PedidoQuit;
    }

}
