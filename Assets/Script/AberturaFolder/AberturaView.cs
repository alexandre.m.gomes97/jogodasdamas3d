﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AberturaView : MonoBehaviour
{
    public delegate void MetodoSemParametros();
    public static event MetodoSemParametros PedidoLogin;
    public static event MetodoSemParametros PedidoRegisto;
    public static event MetodoSemParametros PedidoQuit;

    void Start()
    {
        AberturaModel.RespostaLogin += AberturaModel_RespostaLogin;
        AberturaModel.GoToRegister += AberturaModel_GoToRegister;
        AberturaModel.Quit += AberturaModel_Quit;
    }

    private void AberturaModel_Quit()
    {
        Application.Quit(0);
    }

    private void AberturaModel_GoToRegister()
    {
        SceneManager.LoadScene("Registo", LoadSceneMode.Additive);
    }

    private void AberturaModel_RespostaLogin()
    {
        SceneManager.LoadScene("Login", LoadSceneMode.Additive);
    }

    //BUTTONS START

    public void btn_login()
    {
        if (PedidoLogin != null)
            PedidoLogin();
        
    }

    public void btn_Registo()
    {
       
        if (PedidoRegisto != null)
            PedidoRegisto();

    }

    public void btn_Quit()
    {
        if (PedidoQuit != null)
            PedidoQuit();
    }

    //BUTTONS END


    public void OnDisable()
    {
        AberturaModel.RespostaLogin -= AberturaModel_RespostaLogin;
        AberturaModel.GoToRegister -= AberturaModel_GoToRegister;
        AberturaModel.Quit -= AberturaModel_Quit;
    }
}
