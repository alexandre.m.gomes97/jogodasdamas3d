﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AberturaModel
{
    public delegate void MetodoSemParametros();
    public static event MetodoSemParametros RespostaLogin;
    public static event MetodoSemParametros GoToRegister;
    public static event MetodoSemParametros Quit;

    public void onLoginBtn()
    {
        if (RespostaLogin != null)
            RespostaLogin();
    }

    public void onButtonRegister()
    {
        if (GoToRegister != null)
            GoToRegister();
    }

    public void onButtonQuit()
    {
        if (Quit != null)
            Quit();
    }

}
