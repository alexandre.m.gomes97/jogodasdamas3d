﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CreditsView : MonoBehaviour
{
    public delegate void MetodosSemParametros();
    public static event MetodosSemParametros PedidoReturn;

    // Start is called before the first frame update
    void Start()
    {
        CreditsModel.ReturnB += CreditsModel_ReturnB;

    }

    private void CreditsModel_ReturnB()
    {
        SceneManager.UnloadSceneAsync("Credits");
    }

    //BUTTONS START

    public void btnReturn()
    {
        if (PedidoReturn != null)
            PedidoReturn();
    }

    //BUTTONS END

    public void OnDisable()
    {
        CreditsModel.ReturnB -= CreditsModel_ReturnB;
    }
}
