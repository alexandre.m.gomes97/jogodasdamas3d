﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CreditsController : MonoBehaviour
{
    public CreditsModel CreditsModel { get; set; } = new CreditsModel();


    public void OnEnable()
    {
        CreditsView.PedidoReturn += CreditsView_PedidoReturn;
    }

    private void CreditsView_PedidoReturn()
    {
        Debug.Log("EI");
        CreditsModel.onButtonReturn();
    }

    public void OnDisable()
    {
        CreditsView.PedidoReturn -= CreditsView_PedidoReturn;
    }

}
