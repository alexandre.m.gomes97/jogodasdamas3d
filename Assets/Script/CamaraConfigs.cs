﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JogoDasDamas
{
    public class CamaraConfigs : MonoBehaviour
    {

        public Canvas CanvasM;
        public Camera GameCam;
        // Start is called before the first frame update
        
        public void disableCanvas()
        {
            CanvasM.gameObject.SetActive(false);
        }

        public void enableCanvas()
        {
            CanvasM.gameObject.SetActive(true);
        }

        public void EnableGameCam()
        {
            Camera.main.enabled = false;
            GameCam.enabled = true;
        }

        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
