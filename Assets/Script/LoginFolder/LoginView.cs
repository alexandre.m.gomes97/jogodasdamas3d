﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using System.Data.SqlClient;
using JogoDasDamas;
using UnityEngine.UI;

namespace JogoDasDamas
{
    public class LoginView : MonoBehaviour
    {
        public delegate void MetodoSemParametros();
        public delegate void MetodoComDuasStrings(string username, string pass);
        public static event MetodoSemParametros PedidoCancel;
        public static event MetodoSemParametros PedidoForgotPass;
        public static event MetodoComDuasStrings PedidoLogin;

        public string password;

       

        //UI
        public InputField Username;
        public InputField Password;

        

        void Start()
        {
           
        
            LoginModel.GoBack += LoginModel_GoBack;
            LoginModel.GoToSceneForgotPass += LoginModel_GoToSceneForgotPass;
            LoginModel.LoginSucess += LoginModel_LoginSucess;
            LoginModel.RespostaErro += LoginModel_RespostaErro;
        }


        private void Update()
        {

        }
        private void LoginModel_RespostaErro(string msg)
        {
            MessageBox.messageInfo = msg;
            MessageBox.titleInfo = "Login Failed!";
            SceneManager.LoadSceneAsync("MessageBox", LoadSceneMode.Additive);
        }

        private void LoginModel_LoginSucess()
        {
            
            if (PvPMsgView.loginSecond == false)
            {
                SceneManager.LoadScene("Menu", LoadSceneMode.Single);
            }

            else
            {
                PvPMsgView.loginSecond = false;
                SceneManager.UnloadSceneAsync("Login");
                SceneManager.LoadScene("GameZone", LoadSceneMode.Single);
            }
                
        }

        private void LoginModel_GoToSceneForgotPass()
        {
            SceneManager.LoadScene("ForgotPassword",LoadSceneMode.Additive);
        }

        private void LoginModel_GoBack()
        {
            SceneManager.UnloadSceneAsync("Login");
        }


        //BUTTONS START
        public void Btn_Cancel()
        {
           if(PedidoCancel != null)
            {
                PedidoCancel();
            }
                
        }

        public void Btn_ForgotPass()
        {
            if (PedidoForgotPass != null)
            {
                PedidoForgotPass();
            }
                
        }

        public void Btn_Login()
        {


             if (PedidoLogin != null)
                PedidoLogin(Username.text, Password.text);
        }

 
        public void OnDisable()
        {
            LoginModel.GoBack -= LoginModel_GoBack;
            LoginModel.GoToSceneForgotPass -= LoginModel_GoToSceneForgotPass;
            LoginModel.LoginSucess -= LoginModel_LoginSucess;
            LoginModel.RespostaErro -= LoginModel_RespostaErro;
        }
        
       
    }
}
