﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

namespace JogoDasDamas
{

    public class LoginModel
    {
        public delegate void MetodoSemParametros();
        public static event MetodoSemParametros GoBack;
        public static event MetodoSemParametros GoToSceneForgotPass;
        public static event MetodoSemParametros LoginSucess;
        public static event MetodoComUmaString RespostaErro;
        public static event MetodoComDuasString TryLogin;
        public static event MetodoSemParametros OnLoginSucess;

        public static Player Player1 { get; private set; } = new Player();


        public LoginModel()
        {

        }


        public void onButtonLogin(string username, string pass)
        {
            if (TryLogin != null)
                TryLogin(username, pass);

            if (Player1.username == username)
            {
                if (OnLoginSucess != null)
                    OnLoginSucess();



                if (LoginSucess != null)
                    LoginSucess();

                if (GameSystem.Instance.PlayerLogged == null)
                {
                    GameSystem.Instance.PlayerLogged = Player1.getCopyPlayer();
                }
                else
                {

                    GameSystem.Instance.PlayerLogged2 = Player1.getCopyPlayer();
                }
            }
            else
            {
                throw new Exception("Username or password incorrect");

            }
        }



        public void onButtonCancel()
        {

            if (GoBack != null)
                GoBack();
        }


        public void onButtonForgotPass()
        {
            if (GoToSceneForgotPass != null)
            {
                GoToSceneForgotPass();
            }
        }


        public void SendError(string msg)
        {
            if (RespostaErro != null)
                RespostaErro(msg);
        }



    }

}
