﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;


namespace JogoDasDamas
{
     class LoginController : MonoBehaviour
     {
       
        public LoginModel LoginModel { get; set; } = new LoginModel();
        string[] player;

        public  void OnEnable()
        {
            
            LoginView.PedidoCancel += LoginView_PedidoCancel;
            LoginView.PedidoForgotPass += LoginView_PedidoForgotPass;
            LoginView.PedidoLogin += LoginView_PedidoLogin1;
            LoginModel.TryLogin += LoginModel_TryLogin;
            LoginModel.OnLoginSucess += LoginModel_OnLoginSucess;

            
        }

        private void LoginModel_OnLoginSucess()
        {
            int aux;
            int.TryParse(GetDataValue(player[0], "ID_P:"), out aux);
            LoginModel.Player1.ID_P = aux;
            LoginModel.Player1.username = GetDataValue(player[0], "Username:");
            LoginModel.Player1.password = GetDataValue(player[0], "Password:");
            LoginModel.Player1.name = GetDataValue(player[0], "Nome:");
            LoginModel.Player1.email = GetDataValue(player[0], "Email:");
            LoginModel.Player1.pais = GetDataValue(player[0], "Pais:");
            LoginModel.Player1.photoPath = GetDataValue(player[0], "Foto:");

            int.TryParse(GetDataValue(player[0], "Wins:"), out aux);
            LoginModel.Player1.wins = aux;

            int.TryParse(GetDataValue(player[0], "Defeats:"), out aux);
            LoginModel.Player1.defeats = aux;

            int.TryParse(GetDataValue(player[0], "Draws:"), out aux);
            LoginModel.Player1.draws = aux;

            int.TryParse(GetDataValue(player[0], "N_games:"), out aux);
            LoginModel.Player1.n_games = aux;

            int.TryParse(GetDataValue(player[0], "A_Games:"), out aux);
            LoginModel.Player1.a_games = aux;

            
        }

        private void LoginModel_TryLogin(string user, string pass)
        {
            WWWForm form = new WWWForm();
            form.AddField("LoginPost", user);
            form.AddField("PassPost", pass);
            WWW www = new WWW("http://localhost/Player/PlayerData.php", form);

        
                Thread.Sleep(1000);
            
            
            
           
            string playerDataString = www.text;

            if(playerDataString == "")
                throw new Exception ("Username or password incorrect");
             player = playerDataString.Split(';');

            LoginModel.Player1.username = GetDataValue(player[0], "Username:");
            LoginModel.Player1.password = GetDataValue(player[0], "Password:");
            
           
        }


        string GetDataValue(string data, string index)
        {
            string value = data.Substring(data.IndexOf(index) + index.Length);
            if (value.Contains("|"))
                value = value.Remove(value.IndexOf('|'));
            return value;
        }



        private void LoginView_PedidoLogin1(string username, string pass)
        {
            try
            {
                LoginModel.onButtonLogin(username, pass);
            }
            catch(Exception e)
            {
                LoginModel.SendError(e.Message);
            }
        }


        private void LoginView_PedidoForgotPass()
        {
            
            LoginModel.onButtonForgotPass();
        }

        private void LoginView_PedidoCancel()
        {
            LoginModel.onButtonCancel();
        }

        public void OnDisable()
        {
            LoginView.PedidoCancel -= LoginView_PedidoCancel;
            LoginView.PedidoForgotPass -= LoginView_PedidoForgotPass;
            LoginView.PedidoLogin -= LoginView_PedidoLogin1;
         
        }


    }
}
