﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.SceneManagement;
using UnityEngine;
using System.Collections;


namespace Assets.Script
{
    public class MenuView : MonoBehaviour
    {
        public  delegate void MetodoSemParametros();
        public static event MetodoSemParametros PedidoPlay;
        public static event MetodoSemParametros PedidoQuit;
        public static event MetodoSemParametros PedidoHigh;
        public static event MetodoSemParametros PedidoEditProfile;
        public static event MetodoSemParametros PedidoCredits;
        public static event MetodoSemParametros PedidoServer;
        public static event MetodoSemParametros PedidoLogout;
        

        void Start()
        {
            Debug.Log("Estou no menu");
            MenuModel.MudarDeScene += MenuModel_MudarDeScene;
            MenuModel.GoToSceneHighScores += MenuModel_MudarParaHighScores;
            MenuModel.GoToSceneEditProfile += MenuModel_GoToSceneEditProfile;
            MenuModel.GoToSceneCredits += MenuModel_GoToSceneCredits;
            MenuModel.GoToServerConnection += MenuModel_GoToServerConnection;
            MenuModel.GoToAbertura += MenuModel_GoToAbertura;
            MenuModel.Quit += MenuModel_Quit;
        }

        private void MenuModel_Quit()
        {
            Application.Quit(0);
        }

        private void MenuModel_GoToAbertura()
        {
            SceneManager.LoadScene("Abertura");
        }

        private void MenuModel_GoToServerConnection()
        {
            SceneManager.LoadScene("ServerConection", LoadSceneMode.Additive);
        }

        private void MenuModel_GoToSceneCredits()
        {
            SceneManager.LoadScene("Credits",LoadSceneMode.Additive);
        }

        private void MenuModel_GoToSceneEditProfile()
        {
            SceneManager.LoadScene("PerfilEditor", LoadSceneMode.Additive);
        }

        private void MenuModel_MudarParaHighScores()
        {
            
            SceneManager.LoadScene("Highscores",LoadSceneMode.Additive);
        }

        private void MenuModel_MudarDeScene()
        {
            SceneManager.LoadScene("PlaySubMenu", LoadSceneMode.Additive);
        }

        //BUTTONS START

        public void Btn_Play()
        {
            if (PedidoPlay != null)
                PedidoPlay();
        }

        public void Btn_High()
        {
            if (PedidoHigh != null)
            {
                PedidoHigh();
            }
               
        }

        public void Btn_Quit()
        {
            
            if (PedidoQuit != null)
                PedidoQuit();
        }

        public void Btn_EditProfile()
        {
            if (PedidoEditProfile != null)
                PedidoEditProfile();
        }

        public void Btn_Credits()
        {
            if (PedidoCredits != null)
                PedidoCredits();
        }

        public void Btn_Multiplayer()
        {
            if (PedidoServer != null)
                PedidoServer();
        }

        public void Btn_Logout()
        {
            if (PedidoLogout != null)
                PedidoLogout();
        }
        //BUTTONS END

        public void OnDisable()
        {
            MenuModel.MudarDeScene -= MenuModel_MudarDeScene;
            MenuModel.GoToSceneHighScores -= MenuModel_MudarParaHighScores;
            MenuModel.GoToSceneEditProfile -= MenuModel_GoToSceneEditProfile;
            MenuModel.GoToSceneCredits -= MenuModel_GoToSceneCredits;
            MenuModel.GoToServerConnection -= MenuModel_GoToServerConnection;
            MenuModel.GoToAbertura -= MenuModel_GoToAbertura;
            MenuModel.Quit -= MenuModel_Quit;
        }

    }
}
