﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuModel
{ 
    public delegate void MetodoSemParametros();
    public static event MetodoSemParametros MudarDeScene;
    public static event MetodoSemParametros GoToSceneHighScores;
    public static event MetodoSemParametros GoToSceneEditProfile;
    public static event MetodoSemParametros GoToSceneCredits;
    public static event MetodoSemParametros GoToServerConnection;
    public static event MetodoSemParametros GoToAbertura;
    public static event MetodoSemParametros Quit;

    public void playClick()
    {
        if (MudarDeScene != null)
            MudarDeScene();
    }

    public void onButtonQuit()
    {
        if (Quit != null)
            Quit();
    }

    public void onButtonHighScore()
    {
        if(GoToSceneHighScores != null)
            GoToSceneHighScores();
    }
   
    public void onButtonEditProfile()
    {
        if (GoToSceneEditProfile != null)
        {
            Debug.Log("Vou mandar o evento para a view");
            GoToSceneEditProfile();
        }
            
    }

    public void onButtonCredits()
    {
        if (GoToSceneCredits != null)
            GoToSceneCredits();
    }

    public void onButtonMultiplayer()
    {
        if (GoToServerConnection != null)
            GoToServerConnection();
    }

    public void onButtonLogout()
    {
        if (GoToAbertura != null)
            GoToAbertura();
    }

    

}
