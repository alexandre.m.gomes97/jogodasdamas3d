﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Script.MenuFolder
{
    class MenuController : MonoBehaviour
    {

        public MenuModel MenuModel { get; set; } = new MenuModel();
       
        public void OnEnable()
        {
            MenuView.PedidoPlay += Menu_View_PedidoPlay;
            MenuView.PedidoHigh += MenuView_PedidoHigh;
            MenuView.PedidoEditProfile += MenuView_PedidoEditProfile;
            MenuView.PedidoCredits += MenuView_PedidoCredits;
            MenuView.PedidoServer += MenuView_PedidoServer;
            MenuView.PedidoLogout += MenuView_PedidoLogout;
            MenuView.PedidoQuit += MenuView_PedidoQuit;
        }

        private void MenuView_PedidoQuit()
        {
            MenuModel.onButtonQuit();
        }

        private void MenuView_PedidoLogout()
        {
            MenuModel.onButtonLogout();
        }

        private void MenuView_PedidoServer()
        {
            MenuModel.onButtonMultiplayer();
        }

        private void MenuView_PedidoCredits()
        {
            MenuModel.onButtonCredits();
        }

        private void MenuView_PedidoEditProfile()
        {
            MenuModel.onButtonEditProfile();
        }

        private void Menu_View_PedidoPlay()
        {
            
            MenuModel.playClick();

        }

        private void MenuView_PedidoHigh()
        {
            MenuModel.onButtonHighScore();
        }

        public void OnDisable()
        { 
            MenuView.PedidoPlay -= Menu_View_PedidoPlay;
            MenuView.PedidoHigh -= MenuView_PedidoHigh;
            MenuView.PedidoEditProfile -= MenuView_PedidoEditProfile;
            MenuView.PedidoCredits -= MenuView_PedidoCredits;
            MenuView.PedidoServer -= MenuView_PedidoServer;
            MenuView.PedidoLogout -= MenuView_PedidoLogout;
            MenuView.PedidoQuit -= MenuView_PedidoQuit;
        }

    }
}
