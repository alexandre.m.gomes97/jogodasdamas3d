﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


namespace JogoDasDamas
{

    public class PlayModel
    {
        public delegate void MetodoSemParametros();
        public static event MetodoSemParametros ReturnB;
        public static event MetodoSemParametros RespostaPVP;
        public static event MetodoSemParametros RespostaPVE;
        public static event MetodoSemParametros RespostaLoad;
        public static event MetodoSemParametros FicheiroNotAllowed;

        public void onButtonReturn()
        {

            if (ReturnB != null)
                ReturnB();
        }

        public void onButtonPVP()
        {
            if (RespostaPVP != null)
                RespostaPVP();
        }

        public void onButtonPVE()
        {
            if (RespostaPVE != null)
            {
                RespostaPVE();
                GameSystem.Instance.botOn = true;
                GameSystem.Instance.PlayerLogged2 = null;
            }

        }

        public void onButtonLoad()
        {

            //se houver load
            if (RespostaLoad != null)
                RespostaLoad();
        }

        public void ReturnGame(string msg)
        {
            var checker = msg.Split('(');
            var player = checker[1].Split(')')[0];

            if(player == GameSystem.Instance.PlayerLogged.username)
            {
                GameSystem.Instance.ReturnGame = true;
                SceneManager.LoadScene("GameZone", LoadSceneMode.Single);
            }
            else
            {
                if (FicheiroNotAllowed != null)
                    FicheiroNotAllowed();
            }
            

        }


    }
}

