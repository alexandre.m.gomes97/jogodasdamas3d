﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace JogoDasDamas
{

    public class PlayController : MonoBehaviour
    {
        public PlayModel PlayModel { get; set; } = new PlayModel();


        public void OnEnable()
        {
            PlayView.PedidoReturn += CreditsView_PedidoReturn;
            PlayView.PedidoPVP += PlayView_PedidoPVP;
            PlayView.PedidoPVE += PlayView_PedidoPVE;
            PlayView.PedidoLoad += PlayView_PedidoLoad;
            PlayView.PedidoReturnGame += PlayView_PedidoReturnGame;
        }

        private void PlayView_PedidoReturnGame(string msg)
        {
            PlayModel.ReturnGame(msg);
        }

        private void PlayView_PedidoLoad()
        {
            PlayModel.onButtonLoad();
        }

        private void PlayView_PedidoPVE()
        {
            PlayModel.onButtonPVE();
        }

        private void PlayView_PedidoPVP()
        {
            PlayModel.onButtonPVP();
        }

        private void CreditsView_PedidoReturn()
        {

            PlayModel.onButtonReturn();
        }

        public void OnDisable()
        {
            PlayView.PedidoReturn -= CreditsView_PedidoReturn;
            PlayView.PedidoPVP -= PlayView_PedidoPVP;
            PlayView.PedidoPVE -= PlayView_PedidoPVE;
            PlayView.PedidoLoad -= PlayView_PedidoLoad;
            PlayView.PedidoReturnGame -= PlayView_PedidoReturnGame;
        }

    }
}
