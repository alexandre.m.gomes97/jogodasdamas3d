﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleFileBrowser;
using UnityEngine.SceneManagement;



namespace JogoDasDamas
{
    public class PlayView : MonoBehaviour
    {
        public delegate void MetodosSemParametros();
        public static event MetodosSemParametros PedidoReturn;
        public static event MetodosSemParametros PedidoPVP;
        public static event MetodosSemParametros PedidoPVE;
        public static event MetodosSemParametros PedidoLoad;
        public static event MetodoComUmaString PedidoReturnGame;
        

        public static string FileDir { get; set; }
        // Start is called before the first frame update
        void Start()
        {
            PlayModel.ReturnB += PlayModel_ReturnB;
            PlayModel.RespostaPVP += PlayModel_RespostaPVP;
            PlayModel.RespostaPVE += PlayModel_RespostaPVE;
            PlayModel.RespostaLoad += PlayModel_RespostaLoad;
            PlayModel.FicheiroNotAllowed += PlayModel_FicheiroNotAllowed;
        }

        private void PlayModel_FicheiroNotAllowed()
        {
            MensagemErro("Este ficheiro não pertencem a este utilizador!");
        }

        private void PlayModel_RespostaLoad()
        {
            ChoseFile();

        }

        private void PlayModel_RespostaPVE()
        {
            SceneManager.LoadScene("GameZone", LoadSceneMode.Single);
        }

        private void PlayModel_RespostaPVP()
        {
            SceneManager.LoadScene("PvPMessage", LoadSceneMode.Additive);
        }

        private void PlayModel_ReturnB()
        {
            SceneManager.UnloadSceneAsync("PlaySubMenu");
        }

        public void MensagemErro(string msg)
        {
            MessageBox.messageInfo = msg;
            MessageBox.titleInfo = "Erro no ficheiro escolhido!";
            SceneManager.LoadSceneAsync("MessageBox", LoadSceneMode.Additive);
        }

        public void btnReturn()
        {
            if (PedidoReturn != null)
                PedidoReturn();
        }

        public void btnPVP()
        {
            if (PedidoPVP != null)
                PedidoPVP();
        }

        public void btnPVE()
        {
            if (PedidoPVE != null)
                PedidoPVE();
        }

        public void btnLoad()
        {
            if (PedidoLoad != null)
                PedidoLoad();
        }


        public void ChoseFile()
        {
            FileBrowser.SetFilters(true, new FileBrowser.Filter("Ficheiros XML", ".xml"));
            FileBrowser.SetDefaultFilter(".xml");
            FileBrowser.SetExcludedExtensions(".lnk", ".tmp", ".zip", ".rar", ".exe");
            FileBrowser.AddQuickLink("Users", "C:\\Users", null);
            StartCoroutine(ShowLoadDialogCoroutine());
        }

        IEnumerator ShowLoadDialogCoroutine()
        {
            // Show a load file dialog and wait for a response from user
            // Load file/folder: file, Initial path: default (Documents), Title: "Load File", submit button text: "Load"
            yield return FileBrowser.WaitForLoadDialog(false, null, "Load File", "Load");
            // Dialog is closed
            // Print whether a file is chosen (FileBrowser.Success)
            // and the path to the selected file (FileBrowser.Result) (null, if FileBrowser.Success is false)
            Debug.Log(FileBrowser.Success + " " + FileBrowser.Result);
            FileDir = FileBrowser.Result;
            getFileDir();

        }

        public void getFileDir()
        {
            
            if(FileDir != null)
            {
                if (PedidoReturnGame != null)
                    PedidoReturnGame(FileDir);
            }
            

        }

        public void OnDisable()
        {
            PlayModel.ReturnB -= PlayModel_ReturnB;
            PlayModel.RespostaPVP -= PlayModel_RespostaPVP;
            PlayModel.RespostaPVE -= PlayModel_RespostaPVE;
            PlayModel.RespostaLoad -= PlayModel_RespostaLoad;
            PlayModel.FicheiroNotAllowed -= PlayModel_FicheiroNotAllowed;
        }
    }
}
