﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data;


using System;


namespace JogoDasDamas
{


    public class RegistoModel
    {
        public static event MetodoSemParametros GoBack;
        public static event MetodoSemParametros Save;
        public static event MetodoComUmaString RespostaErro;
        public static event MetodoComUmPlayer SavePlayerData;

     
 

       public RegistoModel()
        {
            
            
    }


        public void onButtonCancel()
        {

            if (GoBack != null)
                GoBack();
        }

        public void onButtonSave(Player player)
        {


            if (player.name == "" || player.email == "" || player.password == "" || player.username == "" || player.pais == "")
                throw new Exception("There is a blank field!");
            else
            {
               if(player.photoPath == "")
                {
                    throw new Exception("Choose a photo!");
                }
            }
            if (SavePlayerData != null)
                SavePlayerData(player);
            



            //Se nao existir 
            if (Save != null)
                Save();
        }


        public void SendError(string msg)
        {
            if (RespostaErro != null)
                RespostaErro(msg);
        }



        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
