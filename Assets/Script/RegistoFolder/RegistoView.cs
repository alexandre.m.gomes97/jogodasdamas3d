﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using UnityEngine.SceneManagement;
using SimpleFileBrowser;


namespace JogoDasDamas
{
    public class RegistoView : MonoBehaviour
    {
        
        public delegate void MetodoSemParametros();
        public delegate void MetodoComUmPlayer(Player p);
        public static event MetodoSemParametros PedidoCancel;
        public static event MetodoComUmPlayer PedidoSave;

        public static Player Player1 { get; private set; } = new Player();

        //UI
        string imagePath;
        public RawImage photo;

        
        public InputField nome;
        public InputField passoword;
        public InputField email;
        public InputField username;
        public InputField country;
        public Dropdown Bday;
        public Dropdown Mday;
        public Dropdown Yday;
        public Dropdown Gender;
        public string day, month, year;
        RegistoView()
        {
            RegistoModel.GoBack += RegistoModel_GoBack;
            RegistoModel.Save += RegistoModel_Save;
            RegistoModel.RespostaErro += RegistoModel_RespostaErro;   
        }

        private void RegistoModel_RespostaErro(string msg)
        {
            username.text = "";
            MessageBox.messageInfo = msg;
            MessageBox.titleInfo = "Error!";
            SceneManager.LoadSceneAsync("MessageBox", LoadSceneMode.Additive);
        }

        private void RegistoModel_Save()
        {

            SceneManager.UnloadSceneAsync("Registo");
        }

        private void RegistoModel_GoBack()
        {
            SceneManager.UnloadSceneAsync("Registo");
        }

        //BUTTONS START

        public void Btn_Cancel()
        {
            if (PedidoCancel != null)
                PedidoCancel();
        }

        public void Btn_Save()
        {
            Player1.name = nome.text;
            Player1.password = passoword.text;
            Player1.email = email.text;
            Player1.username = username.text;
            Player1.pais = country.text;
            if (Gender.itemText.text == "Option A")
                Player1.gender = "Male";
            else
                Player1.gender = "Female";
            day = Bday.options[Bday.value].text;
            month = Mday.options[Mday.value].text;
            year = Yday.options[Yday.value].text;
            Player1.birthday = year + "-" + month + "-" + day;


            if (PedidoSave != null)
                PedidoSave(Player1);

        }

        //FALTA BUTAO SAVE

        //BUTTON END

        public void importImage()
        {
            FileBrowser.SetFilters(true, new FileBrowser.Filter("Images", ".jgp", ".png"));
            FileBrowser.SetDefaultFilter(".jpg");
            FileBrowser.SetExcludedExtensions(".lnk", ".tmp", ".zip", ".rar", ".exe");
            FileBrowser.AddQuickLink("Users", "C:\\Users", null);
            StartCoroutine(ShowLoadDialogCoroutine());
        }
        IEnumerator ShowLoadDialogCoroutine()
        {
            // Show a load file dialog and wait for a response from user
            // Load file/folder: file, Initial path: default (Documents), Title: "Load File", submit button text: "Load"
            yield return FileBrowser.WaitForLoadDialog(false, null, "Load File", "Load");
            // Dialog is closed
            // Print whether a file is chosen (FileBrowser.Success)
            // and the path to the selected file (FileBrowser.Result) (null, if FileBrowser.Success is false)
            Debug.Log(FileBrowser.Success + " " + FileBrowser.Result);
            imagePath = FileBrowser.Result;
            Player1.photoPath = FileBrowser.Result;
            GetImage();
        }

        void GetImage()
        {
            if (imagePath != null)
            {
                UpdateImage();
            }
        }

        void UpdateImage()
        {
            WWW newImage = new WWW("file:///" + imagePath);
            photo.texture = newImage.texture;
        }

        public void OnDisable()
        {
            RegistoModel.GoBack -= RegistoModel_GoBack;
        }
    }
}
