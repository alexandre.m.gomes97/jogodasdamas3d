﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JogoDasDamas;
using System;
using System.Threading;

namespace JogoDasDamas
{
    class RegistoController : MonoBehaviour
    {
        public RegistoModel RegistoModel { get; set; } = new RegistoModel();
        string CreateUserURL = "http://localhost/Player/InsertPlayer.php";
        
        public void OnEnable()
        {
            RegistoView.PedidoCancel += ForgotView_PedidoCancel;
            RegistoView.PedidoSave += RegistoView_PedidoSave;
            RegistoModel.SavePlayerData += RegistoModel_SavePlayerData;
        }

        private void RegistoModel_SavePlayerData(Player player1)
        {
            WWWForm form = new WWWForm();
            form.AddField("usernamePost", player1.username);
            form.AddField("passwordPost", player1.password);
            form.AddField("nomePost", player1.name);
            form.AddField("emailPost", player1.email);
            form.AddField("paisPost", player1.pais);
             player1.photoPath = player1.photoPath.Replace("\\", "\\\\");
            form.AddField("photoPathPost", player1.photoPath);
            form.AddField("birthdayPost", player1.birthday);
            form.AddField("genrePost", player1.gender);

            WWW www = new WWW(CreateUserURL, form);
            Thread.Sleep(500);

            if (www.text == "This username is already in use!")
                throw new System.Exception(www.text);

        }

        private void RegistoView_PedidoSave(JogoDasDamas.Player p)
        {
            try
            {
                RegistoModel.onButtonSave(p);
            }catch(Exception e)
            {
                RegistoModel.SendError(e.Message);
            }
           
        }

        private void ForgotView_PedidoCancel()
        {
            RegistoModel.onButtonCancel();
        }

        public void OnDisable()
        {
            RegistoView.PedidoCancel -= ForgotView_PedidoCancel;
            RegistoView.PedidoSave -= RegistoView_PedidoSave;
            RegistoModel.SavePlayerData -= RegistoModel_SavePlayerData;
        }


    }
}