﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Xml.Linq;

namespace JogoDasDamas
{
    class BoardView : MonoBehaviour
    {
        Utils utl = new Utils();

        //Eventos
        public static event MetodoSemParametros PedidoShowPlayer1;
        public static event MetodoSemParametros PedidoShowPlayer2;
        public static event MetodoSemParametros UpdateValidMoves;
        public static event MetodoMovePiece MovePiece;
        public static event MetodoSemParametros BotTurn;

        
        private static List<GameObject> Squares = new List<GameObject>();

        // Variaveis do UI
        public Dropdown options;
        public RawImage PhotoP1;
        public RawImage PhotoP2;
        public Camera CameraJogo;
        public Text nameP1;
        public Text nameP2;


        public GameObject whitePiecePrefab;
        public GameObject blackPiecePrefab;
        public GameObject redSquare;

        public Text Clock;
        private char characterSpliter = ':';
        private float timer;

        private Vector2 mouseOver;// Guarda as coordenadas do rato

        private void BoardModel_RespostaShowPlayer2()
        {
            SceneManager.LoadScene("Perfil", LoadSceneMode.Additive);
        }
        private void BoardModel_RespostaShowPlayer()
        {
            SceneManager.LoadScene("Perfil", LoadSceneMode.Additive);
        }

        // Start is called before the first frame update

        public void Start()
        {
            if (GameSystem.Instance.ReturnGame)
                ReturnGame();
            else
                GenerateBoard();

            if (!BoardModel.Jogo.Bot)
            {
                nameP2.text = GameSystem.Instance.PlayerLogged2.username;
                WWW newImage2 = new WWW("file:///" + GameSystem.Instance.PlayerLogged2.photoPath);
                PhotoP2.texture =  newImage2.texture;
            }
                
            else
            {
                nameP2.text = GameSystem.Instance.Bot.username;
                WWW newImage2 = new WWW("file:///" + GameSystem.Instance.Bot.photoPath);
                PhotoP2.texture = newImage2.texture;
            }
                

            nameP1.text = GameSystem.Instance.PlayerLogged.username;
            WWW newImage = new WWW("file:///" + GameSystem.Instance.PlayerLogged.photoPath);
            PhotoP1.texture = newImage.texture;

            BoardModel.RespostaShowPlayer += BoardModel_RespostaShowPlayer;
            BoardModel.RespostaShowPlayer2 += BoardModel_RespostaShowPlayer2;
            BoardModel.RespostaMovePiece += BoardModel_RespostaMovePiece;
            BoardModel.CancelMovePiece += BoardModel_CancelMovePiece;
            BoardModel.TransformQueen += BoardModel_TransformQueen;
            BoardModel.EatAPiece += BoardModel_EatAPiece;
            BoardModel.BotNotHaveMoves += BoardModel_BotNotHaveMoves;

            if (!BoardModel.Jogo.Bot)
                nameP2.text = GameSystem.Instance.PlayerLogged2.username;
            else
                nameP2.text = GameSystem.Instance.Bot.username;

            nameP1.text = GameSystem.Instance.PlayerLogged.username;
          
        }

        private void BoardModel_BotNotHaveMoves()
        {
            BoardModel.Jogo.BlackEated = 12;
            EndGame();
        }

        private void BoardModel_EatAPiece()
        {   // Metodo que move as peças para o fundo do tabuleiro apos ser comida
            if (BoardModel.pieceToBeEaten.type == "White")
            {
                BoardModel.pieceToBeEaten.MovePiece(utl.xWhiteOffSet, utl.yWhiteOffSet, utl.boardOffset, utl.pieceOffSet);
                utl.xWhiteOffSet += (float)0.5;
            }
            if (BoardModel.pieceToBeEaten.type == "Black")
            {
                BoardModel.pieceToBeEaten.MovePiece(utl.xBlackOffSet, utl.yBlackOffSet, utl.boardOffset, utl.pieceOffSet);
                utl.xBlackOffSet += (float)0.5;
            }
        }

        private void BoardModel_TransformQueen()
        {   // Metodo que roda a peça pra se tranformar em Queen
            BoardModel.pieces[(int)BoardModel.endDrag.x, (int)BoardModel.endDrag.y].TurnToQueen();
        }

        private void BoardModel_CancelMovePiece()
        {
            if (BoardModel.selectedPiece != null)
                BoardModel.selectedPiece.MovePiece((int)BoardModel.startDrag.x, (int)BoardModel.startDrag.y, utl.boardOffset, utl.pieceOffSet);
            BoardModel.startDrag = Vector2.zero;
            BoardModel.selectedPiece = null;
            ClearSquares();
        }

        private void BoardModel_RespostaMovePiece()
        {   
            BoardModel.selectedPiece.MovePiece(BoardModel.endDrag.x, BoardModel.endDrag.y, utl.boardOffset, utl.pieceOffSet);
            BoardModel.selectedPiece = null;
            ClearSquares();
          }

        // Update is called once per frame
        private void Update()
        {

            timer += Time.deltaTime;
            UpdateClock();

            if(BoardModel.Jogo.WhiteEated >= 12 || BoardModel.Jogo.BlackEated >= 12 || GameSystem.Instance.DrawOn)
            {
                EndGame();
            }

            if(!BoardModel.Jogo.Turn && BoardModel.Jogo.Bot)
            {
                if (BotTurn != null)
                    BotTurn();
            }

            switch (options.value)
            {
                case 0:
                    break;
                case 1:
                    ReturnBtn();
                    options.value = 0;
                    break;
                case 2:
                    SaveBtn();
                    options.value = 0;
                    break;
                case 3:
                    DrawGame();
                    options.value = 0;
                    break;
            }

            UpdateMouseOver();

            if (BoardModel.selectedPiece != null)
                UpdatePieceDrag();


            if (Input.GetMouseButtonDown(0))
            {
                if((int)mouseOver.x != -1 && (int)mouseOver.y != -1)
                    if (BoardModel.Jogo.MakeAPlay(BoardModel.pieces[(int)mouseOver.x, (int)mouseOver.y]))
                    {
                        SelectPiece((int)mouseOver.x, (int)mouseOver.y);
                    }
            }

            if (Input.GetMouseButtonUp(0) && BoardModel.selectedPiece != null)
                TryMove((int)mouseOver.x, (int)mouseOver.y);

        }

        private void TryMove(int xE, int yE)
        {

            if (MovePiece != null)
            {
                MovePiece(xE, yE);
            }


        }

        private void UpdateMouseOver()
        {
            if (!CameraJogo)
            {
                Debug.Log("Unable to find main camera");
                return;
            }

            if (Physics.Raycast(CameraJogo.ScreenPointToRay(Input.mousePosition), out RaycastHit hit, 25.0f, LayerMask.GetMask("Board")))
            {   // Atualiza as coordenadas do rato se for contra a colider box
                mouseOver.x = (int)(hit.point.x - utl.boardOffset.x);
                mouseOver.y = (int)(hit.point.z - utl.boardOffset.z);
            }
            else
            {   // Se o rato estiver fora da colider box as coordenada são (-1,-1)
                mouseOver.x = -1;
                mouseOver.y = -1;
            }
        }

        private void SelectPiece(int x, int y)
        {

            if (x < 0 || x > 7 || y < 0 || y > 7)
                return;//fora do board

            Piece p = BoardModel.pieces[x, y];
            if (p != null)// verifica se a peça existe na matriz
            {
                BoardModel.selectedPiece = p; // atualiza a peça selecionada
                BoardModel.startDrag = mouseOver;
            }

        }

        private void GenerateBoard()
        {
            // Generate White Pieces
            for (int y = 0; y < 3; y++)
            {
                bool oddRow = (y % 2 == 0);
                for (int x = 0; x < 8; x += 2)// Jump one square of the board
                {
                    GeneratePiece((!oddRow) ? x : x + 1, y, type: whitePiecePrefab,false);
                }
            }

            // Generate Black Pieces
            for (int y = 7; y > 4; y--)
            {
                bool oddRow = (y % 2 == 0);
                for (int x = 0; x < 8; x += 2)// Jump one square of the board
                {
                    GeneratePiece((!oddRow) ? x : x + 1, y, blackPiecePrefab,false);
                }
               
            }
        }

        private void GeneratePiece(int x, int y, GameObject type,bool Queen)
        {
            bool isWhite = (type == whitePiecePrefab) ? true : false;
            GameObject go = Instantiate((isWhite) ? whitePiecePrefab : blackPiecePrefab) as GameObject;
            go.transform.SetParent(transform);
            Piece p = go.GetComponent<Piece>();
            p.type = (type == blackPiecePrefab) ? "Black" : "White";
            p.IsQueen = Queen;
            BoardModel.pieces[x, y] = p;
            p.MovePiece(x, y, utl.boardOffset, utl.pieceOffSet);
        }

        public void ReturnBtn()
        {
            Debug.Log("Vou para o menu");
            SceneManager.LoadSceneAsync("Menu", LoadSceneMode.Single);
            GameSystem.Instance.QuitGame = true;
            EndGame();
        }

        public void showPlayerOne()
        {

            if (PedidoShowPlayer1 != null)
            {

                PedidoShowPlayer1();
            }
        }

        public void showPlayerTwo()
        {
            if (PedidoShowPlayer2 != null)
            {

                PedidoShowPlayer2();
            }
        }

        private void UpdatePieceDrag()
        {
            if (!CameraJogo)
            {
                Debug.Log("Unable to find main camera");
                return;
            }

            if (Physics.Raycast(CameraJogo.ScreenPointToRay(Input.mousePosition), out RaycastHit hit, 25.0f, LayerMask.GetMask("Board")))
            {
                BoardModel.selectedPiece.transform.position = hit.point + Vector3.up;
                ShowPossibleMoves();
            }
        }

        public void ClearSquares()
        {
            for (int i = 0; i < Squares.Count; i++)
                Destroy(Squares[i]);
        }

        public void ShowPossibleMoves()
        {
            if (UpdateValidMoves != null)
                UpdateValidMoves();

            for (int i = 0; i < 8; i++)
                for (int j = 0; j < 8; j++)
                    if (BoardModel.validMoves[i, j])
                        GenerateSquare(i, j);
        }

        public void GenerateSquare(int x, int y)
        {
            GameObject square = Instantiate(redSquare, new Vector3(0f, 1.04f, -0.42f), Quaternion.Euler((float)-90.00001, 0, -0.688f));
            square.transform.SetParent(transform);
            square.transform.position = (Vector3.right * x) + (Vector3.forward * y) + utl.boardOffset + utl.pieceOffSet + new Vector3(-0.01f, 0.04f, -0.05f);
            Squares.Add(square);
        }

        public void OnDisable()
        {
            BoardModel.RespostaShowPlayer -= BoardModel_RespostaShowPlayer;
            BoardModel.RespostaShowPlayer2 -= BoardModel_RespostaShowPlayer2;
            BoardModel.RespostaMovePiece -= BoardModel_RespostaMovePiece;
            BoardModel.CancelMovePiece -= BoardModel_CancelMovePiece;
            BoardModel.TransformQueen -= BoardModel_TransformQueen;
            BoardModel.EatAPiece -= BoardModel_EatAPiece;
            BoardModel.BotNotHaveMoves -= BoardModel_BotNotHaveMoves;
        }

        private void UpdateClock()
        {
            float seconds = (timer % 60);
            float minutes = ((int)(timer / 60) % 60);
            float hours = (int)(timer / 3600);

            Clock.text =
                        hours.ToString("00") + characterSpliter
                        + minutes.ToString("00") + characterSpliter
                        + seconds.ToString("00");
        }

        private void EndGame()
        {
            BoardModel.Jogo.Duration = Clock.text;

            BoardModel.Jogo.Bot = false;
            GameSystem.Instance.botOn = false;
            GameSystem.Instance.ReturnGame = false;
            
            BoardModel.Jogo.QuitGame();
        }

        private void SaveBtn()
        {
            if(GameSystem.Instance.PlayerLogged2 == null)
            {
                BoardModel.Jogo.Duration = Clock.text;
                BoardModel.Jogo.SaveGame();
            }
            else
            {
                MessageBox.messageInfo = "Is not allowed to save games against others players!";
                MessageBox.titleInfo = "Not Allowed!";
                SceneManager.LoadSceneAsync("MessageBox", LoadSceneMode.Additive);
            }
    
        }

        private void ReturnGame()
        {
            XDocument doc = XDocument.Load(PlayView.FileDir);


            var registo = from pp in
                          doc.Element("Jogo").Element("Peca").Descendants("Peca")
                          select pp;

            foreach (var campos in registo)
            {
                float.TryParse(campos.Element("X").Value, out float x);
                float.TryParse(campos.Element("Y").Value, out float y);
                string tipo = campos.Attribute("Type").Value;

                Piece novaPiece = new Piece(tipo, x, y);

                if (campos.Element("Dama").Value == "True")
                    novaPiece.IsQueen = true;
                else
                    novaPiece.IsQueen = false;

                if(tipo == "White")
                    GeneratePiece((int)x, (int)y, whitePiecePrefab,novaPiece.IsQueen);
                else
                    GeneratePiece((int)x, (int)y, blackPiecePrefab,novaPiece.IsQueen);

                if (novaPiece.IsQueen)
                    BoardModel.pieces[(int)x, (int)y].TurnToQueen();

            }
        }

        private void DrawGame()
        {
            if(!BoardModel.Jogo.Bot)
                SceneManager.LoadSceneAsync("DrawAsk", LoadSceneMode.Additive);
            else
            {
                MessageBox.messageInfo = "You are not allowed to ask a draw against a bot!";
                MessageBox.titleInfo = "Not Allowed!";
                SceneManager.LoadSceneAsync("MessageBox", LoadSceneMode.Additive);
                EndGame();
            }
        }
    }
}
