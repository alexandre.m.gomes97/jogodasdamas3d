﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace JogoDasDamas
{

    public class BoardModel
    {

        Utils utl = new Utils();

        // Eventos
        public static event MetodoSemParametros RespostaShowPlayer;
        public static event MetodoSemParametros RespostaShowPlayer2;
        public static event MetodoSemParametros RespostaMovePiece;
        public static event MetodoSemParametros TransformQueen;
        public static event MetodoSemParametros CancelMovePiece;
        public static event MetodoSemParametros EatAPiece;
        public static event MetodoSemParametros BotNotHaveMoves;

        public static Player playerToShow { get; private set; }

        public List<Chain> BiggestChain = new List<Chain>();
        public static Vector2 startDrag;   // Guarda as coordenadas iniciais da peça
        public static Vector2 endDrag;
        public static Piece[,] pieces = new Piece[9, 9];
        public static Piece selectedPiece;
        public static List<Piece> possiblePiecesToBeEaten = new List<Piece>();
        public static Piece pieceToBeEaten;
        public bool isEat;
        public static Game Jogo { get; private set; }
        public static Bot Computador { get; private set; }
        public static Vector2 botSelectedMove { get; private set; }
        public static bool[,] validMoves = new bool[9, 9];
        public static bool[,] possibleMoves = new bool[9, 9];
        private bool[,] chainMatrix = new bool[8, 8];
        private bool[,] falseMatrix = new bool[9, 9];
        private bool BigChainUp = false;

        public BoardModel()
        {
            if (GameSystem.Instance.ReturnGame == false)
            {
                Jogo = new Game(GameSystem.Instance.botOn);
                Jogo.WhiteEated = 0;
                Jogo.BlackEated = 0;
            }
            else
            {
                Jogo = new Game(true);
                returnGame();
            }

                if (Jogo.Bot)
                    Computador = new Bot();

            playerToShow = new Player();
            Jogo.initGames = System.DateTime.Now.ToString("MM_dd_HH:mm");

        }

        public void showPlayerInfo()
        {
            if (RespostaShowPlayer != null)
            {
                playerToShow = GameSystem.Instance.PlayerLogged;
                RespostaShowPlayer();
            }

        }

        public void showPlayer2Info()
        {
            if (RespostaShowPlayer2 != null)
            {
                playerToShow = GameSystem.Instance.PlayerLogged2;
                RespostaShowPlayer2();
            }

        }

        public void TryMovePiece(int xE, int yE)
        {
            bool forcedMove = CheckForcedMoves();
            bool normalMove = false;
            Jogo.PieceEated = false;
            if (!forcedMove)
            {
                if (xE != -1 && yE != -1)
                    normalMove = MovePiece(xE, yE);
                else
                    if (CancelMovePiece != null)
                    CancelMovePiece();
            }
            if (xE != -1 && yE != -1)
            {
                if ((forcedMove || normalMove) && validMoves[xE, yE])
                {
                    endDrag.x = xE;
                    endDrag.y = yE;
                    if (RespostaMovePiece != null)
                    {
                        pieceToBeEaten = ReturnPieceToBeEaten(xE, yE);
                        pieces[xE, yE] = selectedPiece;
                        pieces[(int)startDrag.x, (int)startDrag.y] = null;
                        RespostaMovePiece();

                        if (!BigChainUp)
                            Jogo.Turn = !Jogo.Turn;

                        if (EatAPiece != null && pieceToBeEaten != null)
                        {
                            Jogo.PieceEated = true;

                            if (pieces[(int)pieceToBeEaten.X, (int)pieceToBeEaten.Y].type == "White")
                                Jogo.WhiteEated++;
                            else if (pieces[(int)pieceToBeEaten.X, (int)pieceToBeEaten.Y].type == "Black")
                                Jogo.BlackEated++;

                            pieces[(int)pieceToBeEaten.X, (int)pieceToBeEaten.Y] = null;
                            EatAPiece();
                        }

                        if (TurnToQueen(xE, yE) && !pieces[xE, yE].IsQueen)
                        {
                            TransformQueen();
                            pieces[xE, yE].IsQueen = true;
                        }
                    }
                }
                else
                {
                    if (CancelMovePiece != null)
                        CancelMovePiece();
                }
            }
            else
            {
                if (CancelMovePiece != null)
                    CancelMovePiece();
            }
        }

        public bool MovePiece(int xE, int yE)
        {
            validMoves = (bool[,])falseMatrix.Clone();
            if (!selectedPiece.IsQueen)
            {
                if (CalculateValidMovesPiece())
                    return validMoves[xE, yE];
            }
            else
            {
                CalculateValidMovesQueen();
                return validMoves[xE, yE];
            }
            return false;
        }

        public bool CheckForcedMoves()
        {
            possiblePiecesToBeEaten.Clear();
            validMoves = (bool[,])falseMatrix.Clone();
            possibleMoves = (bool[,])falseMatrix.Clone();
            bool forcedEat = false;
            bool isWhite = selectedPiece.type == "White";
            int inc = (isWhite) ? 1 : -1;
            validMoves = (bool[,])falseMatrix.Clone();
            for (int x = 0; x < 8; x++)
                for (int y = 0; y < 8; y++)
                {
                    if (pieces[x, y] != null)
                    {
                        if (pieces[x, y].type == selectedPiece.type && !pieces[x, y].IsQueen)
                        {
                            if (pieces[x + 1, y + inc] != null && y + inc + inc > -1 && y + inc + inc < 8 && x + 2 < 8)
                                if (pieces[x + 1, y + inc].type != selectedPiece.type && pieces[x + 2, y + inc + inc] == null)
                                {
                                    if ((int)selectedPiece.X == x && (int)selectedPiece.Y == y)
                                    {
                                        validMoves[x + 2, y + inc + inc] = true;
                                        possiblePiecesToBeEaten.Add(pieces[x + 1, y + inc]);
                                    }
                                    possibleMoves[x + 2, y + inc + inc] = true;
                                    forcedEat = true;
                                }
                            if (x - 1 > -1 && y + inc + inc > -1 && y + inc + inc < 8 && x - 2 > -1 && x - 2 > -1)
                                if (pieces[x - 1, y + inc] != null)
                                    if (pieces[x - 1, y + inc].type != selectedPiece.type && pieces[x - 2, y + inc + inc] == null)
                                    {
                                        if ((int)selectedPiece.X == x && (int)selectedPiece.Y == y)
                                        {
                                            validMoves[x - 2, y + inc + inc] = true;
                                            possiblePiecesToBeEaten.Add(pieces[x - 1, y + inc]);
                                        }
                                        possibleMoves[x - 2, y + inc + inc] = true;
                                        forcedEat = true;
                                    }
                        }
                        else if (pieces[x, y].type == selectedPiece.type && pieces[x, y].IsQueen)
                        {

                            forcedEat = CheckQueenForcedMove(x, y, forcedEat, 1, 1, 8, 8);
                            forcedEat = CheckQueenForcedMove(x, y, forcedEat, -1, -1, -1, -1);
                            forcedEat = CheckQueenForcedMove(x, y, forcedEat, 1, -1, 8, -1);
                            forcedEat = CheckQueenForcedMove(x, y, forcedEat, -1, 1, -1, 8);
                        }
                    }

                }
            CheckBiggestChain();
            return forcedEat;
        }

        public bool TurnToQueen(int xE, int yE)
        { // Verifica apos um movimento da peça, se esta em condições para se tornar em Queen
            if (yE == 7 && pieces[xE, yE].type == "White")
                return true;
            if (yE == 0 && pieces[xE, yE].type == "Black")
                return true;
            return false;
        }

        public bool CalculateValidMovesPiece()
        {
            /* Metodo para calcular as possiveis posições da peça e poder mostrar ao jogador */
            // Busca as variaveis da peça em questão
            int x = (int)selectedPiece.X;
            bool isWhite = (selectedPiece.type == "White");
            int y = (isWhite) ? (int)selectedPiece.Y + 1 : (int)selectedPiece.Y - 1; // verifica se é branca ou preta para a posição do y
            int yEat = (isWhite) ? y + 1 : y - 1; // calcula a posição correta do salto em caso de ser posssivel comer

            if (y > 7 || y < 0) //Verifica se o y esta dentro das fronteiras do board
                return false;
            if (BoardModel.pieces[x + 1, y] == null)
                validMoves[x + 1, y] = true;

            if (x - 1 > -1)
                if (BoardModel.pieces[x - 1, y] == null)
                    validMoves[x - 1, y] = true;
            return true;
        }

        public void CalculateValidMovesQueen()
        {
            CalculateFreePath(8, 8, 1, 1);
            CalculateFreePath(-1, -1, -1, -1);
            CalculateFreePath(8, -1, 1, -1);
            CalculateFreePath(-1, 8, -1, 1);
        }

        public void CalculateFreePath(int condition1, int condition2, int inc1, int inc2)
        {
            bool freePath = true;
            int numPieces = 0;
            for (int x = (int)selectedPiece.X + inc1, y = (int)selectedPiece.Y + inc2; x != condition1 && y != condition2 && freePath; x += inc1, y += inc2)
            {
                if (pieces[x, y] == null)
                    validMoves[x, y] = true;
                else
                {
                    if (pieces[x, y].type == selectedPiece.type)
                        freePath = false;
                    numPieces++;
                }
                if (numPieces > 1)
                    freePath = false;
            }
        }

        public Piece ReturnPieceToBeEaten(int xE, int yE)
        {
            if (!selectedPiece.IsQueen)
            {
                int positionToEatX = (int)startDrag.x;
                int positionToEatY = (int)startDrag.y;

                if ((int)startDrag.x - xE < 0)
                    positionToEatX += 1;
                else if ((int)startDrag.x > 0)
                    positionToEatX -= 1;
                if ((int)startDrag.y - yE < 0)
                    positionToEatY += 1;
                else if ((int)startDrag.y > 0)
                    positionToEatY -= 1;

                foreach (Piece pp in possiblePiecesToBeEaten)
                {
                    if ((int)pp.X == positionToEatX && (int)pp.Y == positionToEatY)
                        return pp;
                }

            }
            else if (selectedPiece.IsQueen)
            {
                Piece p = null;
                foreach (Piece pp in possiblePiecesToBeEaten)
                {
                    if (((int)pp.X + (int)pp.Y) == (xE + yE) && (((int)selectedPiece.Y + (int)selectedPiece.X) == ((int)pp.X + (int)pp.Y)))
                    {
                        if (p == null)
                            p = pp;
                        if (System.Math.Abs((int)pp.X - (int)xE) < System.Math.Abs((int)p.X - (int)xE) && System.Math.Abs((int)pp.Y - (int)yE) < System.Math.Abs((int)p.Y - (int)yE))
                        {
                            p = pp;
                        }

                    }
                    else if (System.Math.Abs(((int)pp.X + (int)pp.Y) - (xE + yE)) % 2 == 0)
                    {
                        if (p == null)
                            p = pp;
                        if (System.Math.Abs((int)pp.X - (int)xE) < System.Math.Abs((int)p.X - (int)xE) && System.Math.Abs((int)pp.Y - (int)yE) < System.Math.Abs((int)p.Y - (int)yE))
                        {
                            p = pp;
                        }
                    }

                }
                return p;
            }

            return null;
        }

        public bool CheckQueenForcedMove(int x, int y, bool forcedEat, int incX, int incY, int condX, int condY)
        {

            int i = x + incX;
            int j = y + incY;
            bool sameTypeFound = false;
            bool pieceFound = false;
            int numPieces = 0;

            while (i != condX && j != condY)
            {
                if (pieces[i, j] != null && i + incX != condX && j + incY != condY)
                {
                    numPieces++;
                    if (pieces[i, j].type != selectedPiece.type && pieces[i + incX, j + incY] == null && !sameTypeFound && !pieceFound && (pieces[i - incX, j - incY] == pieces[x, y] || pieces[i - incX, j - incY] == null))
                    {
                        if ((int)selectedPiece.X == x && (int)selectedPiece.Y == y)
                        {
                            possiblePiecesToBeEaten.Add(pieces[i, j]);
                            pieceFound = validMoves[i + incX, j + incY] = true;
                        }

                        forcedEat = true;
                    }
                    else if (pieces[i, j].type == selectedPiece.type && i != x && j != y)
                        sameTypeFound = true;
                }
                else if (pieceFound && pieces[i, j] == null)
                {
                    if (numPieces == 1)
                        validMoves[i, j] = true;
                }
                i += incX;
                j += incY;
            }
            return forcedEat;
        }

        public void CheckBiggestChain()
        {
            BiggestChain.Clear();
            BigChainUp = false;
            bool isWhite = selectedPiece.type == "White";
            int inc = (isWhite) ? 1 : -1;
            Chain TempChain = new Chain();

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (possibleMoves[i, j])
                    {
                        TempChain.xStart = i;
                        TempChain.yStart = j;
                        if (CheckeEatConditionPiece(i, j))
                        {
                            TempChain.size++;
                            for (int k = 0; k < 8; k++)
                                for (int l = 0; l < 8; l++)
                                {
                                    if (chainMatrix[k, l])
                                    {
                                        if (CheckeEatConditionPiece(k, l))
                                            TempChain.size++;
                                    }

                                }
                        }
                    }
                    if (TempChain.size > 1 && TempChain.xStart == i && TempChain.yStart == j)
                    {
                        BiggestChain.Add(new Chain(TempChain.size, TempChain.xStart, TempChain.yStart));
                        TempChain.size = 1;
                    }

                }
            }

            if (BiggestChain.Count != 0)
            {
                int maxChain = 0;
                foreach (Chain cc in BiggestChain)
                {
                    if (cc.size > maxChain)
                        maxChain = cc.size;
                }
                validMoves = (bool[,])falseMatrix.Clone();
                for (int a = 0; a < BiggestChain.Count; a++)
                {
                    if (BiggestChain[a].size == maxChain && maxChain > 0)
                        if (selectedPiece.Y + inc + inc == BiggestChain[a].yStart && (selectedPiece.X + 2 == BiggestChain[a].xStart || selectedPiece.X - 2 == BiggestChain[a].xStart) &&
                            (pieces[(int)selectedPiece.X + 1, (int)selectedPiece.Y + inc] != null || pieces[(int)selectedPiece.X - 1, (int)selectedPiece.Y + inc] != null))
                        {
                            validMoves[BiggestChain[a].xStart, BiggestChain[a].yStart] = true;
                            BigChainUp = true;
                        }

                }
            }

        }

        public bool CheckeEatConditionPiece(int x, int y)
        {
            bool forcedEat = false;
            bool isWhite = selectedPiece.type == "White";
            int inc = (isWhite) ? 1 : -1;
            chainMatrix = (bool[,])falseMatrix.Clone();

            if (y + inc + inc > -1 && y + inc + inc < 8 && x + 2 < 8)
                if (pieces[x + 1, y + inc] != null)
                {
                    if (pieces[x + 1, y + inc].type != selectedPiece.type && pieces[x + 2, y + inc + inc] == null)
                    {
                        chainMatrix[x + 2, y + inc + inc] = true;
                        forcedEat = true;
                    }
                }
            if (x - 1 > -1 && y + inc + inc > -1 && y + inc + inc < 8 && x - 2 > -1 && x - 2 > -1)
                if (pieces[x - 1, y + inc] != null)
                    if (pieces[x - 1, y + inc].type != selectedPiece.type && pieces[x - 2, y + inc + inc] == null)
                    {
                        chainMatrix[x - 2, y + inc + inc] = true;
                        forcedEat = true;
                    }

            return forcedEat;
        }

        public void CalculateValidMoves()
        {
            validMoves = (bool[,])falseMatrix.Clone();
            if (!CheckForcedMoves())
            {
                if (!selectedPiece.IsQueen)
                    CalculateValidMovesPiece();
                else
                    CalculateValidMovesQueen();
            }
        }

        public void BotMove()
        {
            botSelectedMove = new Vector2();
            int count = 0;
            do
            {
                selectedPiece = Computador.SelectPiece(pieces);
                CalculateValidMoves();
                botSelectedMove = Computador.GenerateMove(validMoves);
                count++;

            } while (botSelectedMove.x == -1 && botSelectedMove.y == -1 &&  count < 100000);
            if (count >= 100000)
            {
                if (BotNotHaveMoves != null)
                    BotNotHaveMoves();
            }
            else
            {
                startDrag = new Vector2((int)selectedPiece.X, (int)selectedPiece.Y);
                TryMovePiece((int)botSelectedMove.x, (int)botSelectedMove.y);

            }

        }

        public void returnGame()
        {
            pieces = Jogo.returnGame(PlayView.FileDir);
        }
    }
}




