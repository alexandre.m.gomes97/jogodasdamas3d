﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Xml.Linq;


namespace JogoDasDamas
{
    public class Game : MonoBehaviour
    {
        private int id;
        private string duration;
        private int whiteEated;
        private int blackEated;
        private bool whiteTurn;
        private bool blackTurn;
        private bool pieceEated;
        private char[] playerName;

        public bool Bot { get; set; }
        public int Id { get => id; set => id = value; }
        public string Winner { get; set; }
        public string Loser { get; set; }
        public string Duration { get => duration; set => duration = value; }
        public int WhiteEated { get => whiteEated; set => whiteEated = value; }
        public int BlackEated { get => blackEated; set => blackEated = value; }
        public bool PieceEated { get => pieceEated; set => pieceEated = value; }
        public Player P1 { get; set; }
        public Player P2 { get; set; }
        public bool Turn { get; set; } // True para White -- False para black
        public static Piece[,] pieces;
        public string initGames { get; set; }
        public Game(bool _bot)
        {
            Bot = _bot;
            P1 = new Player();

            P1.pieceType = "White";
            if (!Bot)
            {
                P2 = new Player();
                P2.pieceType = "Black";
            }

            WhiteEated = 0;
            BlackEated = 0;

            Turn = true;
        }

        public void QuitGame()
        {
            if(!GameSystem.Instance.DrawOn && !GameSystem.Instance.QuitGame)
            {
                if (this.BlackEated == 12)
                {
                    this.Winner = GameSystem.Instance.PlayerLogged.username;
                    this.Loser = (GameSystem.Instance.PlayerLogged2 != null) ? GameSystem.Instance.PlayerLogged2.username : "Computador";
                }
                else if (this.WhiteEated == 12)
                {
                    this.Winner = (GameSystem.Instance.PlayerLogged2 != null) ? GameSystem.Instance.PlayerLogged2.username : "Computador";
                    this.Loser = GameSystem.Instance.PlayerLogged.username;

                }
                else
                    this.Winner = "Bot";


                GameSystem.Instance.Winner = this.Winner;
                GameSystem.Instance.PiecesRemain = (this.WhiteEated < this.BlackEated) ? (12 - this.WhiteEated) : (12 - this.BlackEated);
                GameSystem.Instance.duration = this.Duration;
                GameSystem.Instance.Loser = this.Loser;
                GameSystem.Instance.DrawOn = false;
                BoardModel.Jogo.Bot = false;
                GameSystem.Instance.botOn = false;
                GameSystem.Instance.initGame = initGames;
                SceneManager.LoadSceneAsync("Victory", LoadSceneMode.Single);
            }
            else if(GameSystem.Instance.DrawOn)
            {
                GameSystem.Instance.duration = this.Duration;
                GameSystem.Instance.Loser = this.Loser;
                BoardModel.Jogo.Bot = false;
                GameSystem.Instance.botOn = false;
                GameSystem.Instance.initGame = initGames;
                SceneManager.LoadSceneAsync("Draw", LoadSceneMode.Additive);
                GameSystem.Instance.DrawOn = false;
                GameSystem.Instance.PlayerLogged.draws++;
                GameSystem.Instance.PlayerLogged2.draws++;
            }else
            {
                ResetGameSystem();
            }
        }

        public void ResetGameSystem()
        {
            GameSystem.Instance.DrawOn = false;
            GameSystem.Instance.botOn = false;
            GameSystem.Instance.PlayerLogged2 = null;
            GameSystem.Instance.ReturnGame = false;
            GameSystem.Instance.Winner = "";
            GameSystem.Instance.Loser = "";
            GameSystem.Instance.QuitGame = false;
    }

        public void SaveGame()
        {
            string m_Path = Application.dataPath;
            XDocument doc = new XDocument(new XDeclaration("1.0", Encoding.UTF8.ToString(), "yes"),
              new XElement("Jogo",
              new XElement("Peca"),
              new XElement("GameData")
              )
           );

            XElement gameData = new XElement("GameData", new XAttribute("Duration", this.duration)
                                            , new XElement("BlackEated", this.blackEated.ToString())
                                            , new XElement("WhiteEated", this.whiteEated.ToString())
                                            , new XElement("Turn", this.Turn.ToString())
                                            );
            doc.Element("Jogo").Element("GameData").Add(gameData);
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    if (BoardModel.pieces[i, j] != null)
                    {
                        XElement peca = new XElement("Peca", new XAttribute("Type", BoardModel.pieces[i, j].type)
                                                  , new XElement("X", BoardModel.pieces[i, j].X.ToString())
                                                  , new XElement("Y", BoardModel.pieces[i, j].Y.ToString())
                                                  , new XElement("Dama", BoardModel.pieces[i, j].IsQueen.ToString())
                                                  );
                        doc.Element("Jogo").Element("Peca").Add(peca);
                    }


                }

            }


            doc.Save(m_Path + "\\" + System.DateTime.Now.ToString("MM_dd_HH#mm") + "(" + GameSystem.Instance.PlayerLogged.username + ")" + ".xml");

        }

        public bool MakeAPlay(Piece p)
        {
            if (p != null)
            {
                if (P1.pieceType == p.type && Turn)
                {
                    return true;
                }
                else if (!Bot)
                {
                    if (P2.pieceType == p.type && !Turn && !Bot)
                        return true;
                    else
                        return false;
                }
                else
                    return false;

            }
            else
                return false;

        }

        public void CheckTie()
        {

        }

        public Piece[,] returnGame(string filePath)
        {
            pieces = new Piece[9, 9];

            XDocument doc = XDocument.Load(filePath);

            
            var registo = from pp in
                          doc.Element("Jogo").Element("Peca").Descendants("Peca")
                          select pp;

            foreach (var campos in registo)
            {
                float.TryParse(campos.Element("X").Value, out float x);
                float.TryParse(campos.Element("Y").Value, out float y);

                Piece novaPiece = new Piece(campos.Attribute("Type").Value, x, y);

                if (campos.Element("Dama").Value == "True")
                    novaPiece.IsQueen = true;
                else
                    novaPiece.IsQueen = false;
                pieces[(int)x, (int)y] = novaPiece;
                
            }
            registo = from dataGame in
                      doc.Element("Jogo").Element("GameData").Descendants("GameData")
                      select dataGame;

            foreach( var campos in registo)
            {
                duration = campos.Attribute("Duration").Value;
                int.TryParse(campos.Element("BlackEated").Value, out int resultB);
                int.TryParse(campos.Element("WhiteEated").Value, out int resultW);
                blackEated = resultB;
                whiteEated = resultW;

                if (campos.Element("Turn").Value == "True")
                    Turn = true;

            }
            return pieces; 
        }

 
    }
}
