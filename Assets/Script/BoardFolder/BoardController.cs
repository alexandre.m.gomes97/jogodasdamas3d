﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JogoDasDamas
{
    public class BoardController : MonoBehaviour
    {
        public BoardModel BoardModel { get; set; } = new BoardModel();

        public void OnEnable()
        {
            BoardView.PedidoShowPlayer1 += BoardView_PedidoShowPlayer1;
            BoardView.PedidoShowPlayer2 += BoardView_PedidoShowPlayer2;
            BoardView.MovePiece += BoardView_MovePiece;
            BoardView.UpdateValidMoves += BoardView_UpdateValidMoves;
            BoardView.BotTurn += BoardView_BotTurn;
        }


        private void BoardView_BotTurn()
        {
            BoardModel.BotMove();
        }

        private void BoardView_UpdateValidMoves()
        {
            BoardModel.CalculateValidMoves();
        }

        private void BoardView_MovePiece(int xE, int yE)
        {
            BoardModel.TryMovePiece(xE, yE);
        }

        private void BoardView_PedidoShowPlayer2()
        {
            BoardModel.showPlayer2Info();
        }

        private void BoardView_PedidoShowPlayer1()
        {
            BoardModel.showPlayerInfo();
        }

        public void OnDisable()
        {
            BoardView.PedidoShowPlayer1 -= BoardView_PedidoShowPlayer1;
            BoardView.PedidoShowPlayer2 -= BoardView_PedidoShowPlayer2;
            BoardView.MovePiece -= BoardView_MovePiece;
            BoardView.UpdateValidMoves -= BoardView_UpdateValidMoves;
            BoardView.BotTurn -= BoardView_BotTurn;
        }
    
    }
}

 