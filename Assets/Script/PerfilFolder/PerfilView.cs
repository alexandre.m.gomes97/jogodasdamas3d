﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;


namespace JogoDasDamas
{
    public class PerfilView : MonoBehaviour
    {
        public delegate void MetodoSemParametros();
        public static event MetodoSemParametros PedidoReturn;


        public Text PlayerName;
        public Text PlayerEmail;
        public Text PlayerCountry;
        public Text Username;
        public Text Victorys;
        public Text Defeats;
        public Text Draws;
        public Text AbandonedGames;
        public Text GamesStarted;
        public RawImage photo;

        public Text n_vitories;
        public Text n_defeats;
        public Text n_draws;
        public Text n_games;
        public Text n_agames;
        public Text Vic_P;
        public Player playerShow { get; set; } 

       

        private void PerfilModel_RespostaReturn()
        {
            SceneManager.UnloadSceneAsync("Perfil");
        }

       

        // Start is called before the first frame update
        void Start()
        {
            PerfilModel.RespostaReturn += PerfilModel_RespostaReturn;
            playerShow = BoardModel.playerToShow;
            
        }

        // Update is called once per frame
        void Update()
        {
            UpdateLabels();
        }

        void UpdateLabels()
        {
            PlayerName.text = playerShow.name;
            PlayerEmail.text = playerShow.email;
            PlayerCountry.text = playerShow.pais;
            Username.text = playerShow.username;
            WWW newImage = new WWW("file:///" + playerShow.photoPath);
            photo.texture = newImage.texture;


            n_vitories.text = playerShow.wins.ToString();
            n_defeats.text = playerShow.defeats.ToString();
            n_draws.text = playerShow.draws.ToString();
            n_games.text = playerShow.n_games.ToString();
            n_agames.text = playerShow.a_games.ToString();
            playerShow.WinPercentageCalc();
            Vic_P.text = playerShow.WinPercentage.ToString() + '%';
        }

        //BUTTONS START
        public void ReturnBtn()
        {
            if (PedidoReturn != null)
                PedidoReturn();
        }


        //BUTTONS END

        public void OnDisable()
        {
            PerfilModel.RespostaReturn -= PerfilModel_RespostaReturn;
        }

    }
}


