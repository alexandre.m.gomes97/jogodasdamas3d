﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JogoDasDamas
{
    public class PerfilModel 
    {
        public delegate void MetodoSemParametros();
        public static event MetodoSemParametros RespostaReturn;

        public void returnToGame()
        {
            if (RespostaReturn != null)
                RespostaReturn();
        }

    }
}

