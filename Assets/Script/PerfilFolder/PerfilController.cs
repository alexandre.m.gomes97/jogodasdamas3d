﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace JogoDasDamas
{
    public class PerfilController : MonoBehaviour
    {

        public PerfilModel PerfilModel { get; set; } = new PerfilModel();
        public void OnEnable()
        {
            PerfilView.PedidoReturn += PerfilView_PedidoReturn;
        }

        private void PerfilView_PedidoReturn()
        {
            PerfilModel.returnToGame();
        }

        public void OnDisable()
        {
            PerfilView.PedidoReturn -= PerfilView_PedidoReturn;
        }
    }
}

