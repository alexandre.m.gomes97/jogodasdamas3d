﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piece : MonoBehaviour
{
    public double X { get; set; }
    public double Y { get; set; }
    public string type { get; set; }
    public bool IsQueen { get; set; } = false;
    public bool IsForced { get; set; } = false;

    public Piece()
    {

    }

    public Piece(int x, int y)
    {
        X = x;
        Y = y;
    }

    public Piece(string type,double x, double y)
    {
        this.type = type;
        X = x;
        Y = y;
    }

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void MovePiece(float x, float y, Vector3 boardOffSet, Vector3 pieceOffSet)
    {
        X = x; Y = y;
        this.transform.position = (Vector3.right * x) + (Vector3.forward * y) + boardOffSet + pieceOffSet;
    }

    public void TurnToQueen()
    {
        this.transform.Rotate(Vector3.right * 180);
    }

}
