﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using System.Linq;

namespace JogoDasDamas
{
    public class HighScoreModel
    {
        public delegate void MetodoSemParametros();
        public static event MetodoSemParametros GoBack;
        public static event MetodoSemParametros ShowHigh;
        public static List<Player> listaP { get; set; }


        public HighScoreModel()
        {
            listaP = new List<Player>();

        }

        public void onButtonCancel()
        {

            if (GoBack != null)
                GoBack();
        }


        public void Acesso()
        {
            WWW www = new WWW("http://localhost/Player/GetAllWins.php");


            Thread.Sleep(1000);

            string playerDataString = www.text;

            if (playerDataString == "")
                throw new Exception("Username or password incorrect");
            string[] players = playerDataString.Split(';');

            for (int i = 0; i < players.Length - 1; i++)
            {
                int.TryParse(GetDataValue(players[i], "Wins:"), out int win);
                int.TryParse(GetDataValue(players[i], "N_games:"), out int Ngame);
                Player pl = new Player(GetDataValue(players[i], "Username:"), win, Ngame);
                listaP.Add(pl);
            }



            var list = listaP.OrderByDescending(x => x.WinPercentage)
             .ToList();
            listaP.Clear();
            listaP = list;
            if (ShowHigh != null)
                ShowHigh();

        }

        string GetDataValue(string data, string index)
        {
            string value = data.Substring(data.IndexOf(index) + index.Length);
            if (value.Contains("|"))
                value = value.Remove(value.IndexOf('|'));
            return value;
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}