﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JogoDasDamas
{
    class HighScoreController : MonoBehaviour
    {
        public HighScoreModel HighScoreModel { get; set; } = new HighScoreModel();

        public void OnEnable()
        {
            HighScoreView.PedidoCancel += ForgotView_PedidoCancel;
            HighScoreView.ReceiveData += HighScoreView_ReceiveData;
        }

        private void HighScoreView_ReceiveData()
        {
            HighScoreModel.Acesso();
        }

        private void ForgotView_PedidoCancel()
        {
           HighScoreModel.onButtonCancel();
        }

        public void OnDisable()
        {
            HighScoreView.PedidoCancel -= ForgotView_PedidoCancel;
            HighScoreView.ReceiveData -= HighScoreView_ReceiveData;
        }





    }
}
