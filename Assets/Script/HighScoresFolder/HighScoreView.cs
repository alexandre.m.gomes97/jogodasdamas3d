﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

namespace JogoDasDamas
{
    public class HighScoreView : MonoBehaviour
    {
        public delegate void MetodoSemParametros();
        public static event MetodoSemParametros PedidoCancel;
        public static event MetodoSemParametros ReceiveData;

        public Text FirstPlace;
        public Text FirstWin;

        public Text SecondPlace;
        public Text SecondWin;

        public Text ThirdPlace;
        public Text ThirdWin;

        public Text FourthPlace;
        public Text FourthWin;

        public Text FifthPlace;
        public Text FifthWin;



        void Start()
        {

            if (ReceiveData != null)
                ReceiveData();
        }



        HighScoreView()
        {
            HighScoreModel.GoBack += LoginModel_GoBack;
            HighScoreModel.ShowHigh += HighScoreModel_ShowHigh;
        }

        private void HighScoreModel_ShowHigh()
        {
            FirstPlace.text = HighScoreModel.listaP[0].username;
            FirstWin.text = HighScoreModel.listaP[0].WinPercentage.ToString("f2") +'%';

            SecondPlace.text = HighScoreModel.listaP[1].username;
            SecondWin.text = HighScoreModel.listaP[1].WinPercentage.ToString("f2") + '%';

            ThirdPlace.text = HighScoreModel.listaP[2].username;
          ThirdWin.text = HighScoreModel.listaP[2].WinPercentage.ToString("f2") + '%';

            FourthPlace.text = HighScoreModel.listaP[3].username;
            FourthWin.text = HighScoreModel.listaP[3].WinPercentage.ToString("f2") + '%';

            FifthPlace.text = HighScoreModel.listaP[4].username;
            FifthWin.text = HighScoreModel.listaP[4].WinPercentage.ToString("f2") + '%';
    }

        private void LoginModel_GoBack()
        {
            SceneManager.UnloadSceneAsync("Highscores");
        }

        public void Btn_ReturnCredits()
        {
            SceneManager.UnloadSceneAsync("Credits");
        }

        //BUTTONS START
        public void Btn_Cancel()
        {
            if (PedidoCancel != null)
                PedidoCancel();


          

       
        }
        //BUTTONS END

        public void OnDisable()
        {
            HighScoreModel.GoBack -= LoginModel_GoBack;
            HighScoreModel.ShowHigh -= HighScoreModel_ShowHigh;
        }
    }
}
