﻿using UnityEngine;
using System.Collections;
using System;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Linq;

namespace JogoDasDamas
{

    public class ForgotModel
    {
        private static System.Random random = new System.Random();
        public delegate void MetodoSemParametros();
        public static event MetodoSemParametros GoBack;
        public static event MetodoSemParametros RecoverPass;
        public static event MetodoComUmaString OnErro;
        public static event MetodoComTresString SendEmail;

        public void onButtonCancel()
        {

            if (GoBack != null)
                GoBack();
        }

        public void onButtonRecover(string email, string username)
        {
            Email(email, username);
            if (RecoverPass != null)
                RecoverPass();
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void onError(string msg)
        {
            if (OnErro != null)
                OnErro(msg);
        }

        public void Email(string email, string username)
        {
            var pass = Pass(5);
            if (SendEmail != null)
                SendEmail( email,  username, pass);

            
            MailMessage mail = new MailMessage();

            mail.From = new MailAddress("damaslab32@gmail.com");
            if(email.Contains('@'))
                mail.To.Add(email);
            else
            {
                throw new Exception("Email not valid");
            }
            mail.Subject = "Recover pass";
            mail.Body = "NEW PASSWORD: " + pass;

            SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
            smtpServer.Port = 587;
            smtpServer.Credentials = new System.Net.NetworkCredential("damaslab32@gmail.com", "fortnite") as ICredentialsByHost;
            smtpServer.EnableSsl = true;
            ServicePointManager.ServerCertificateValidationCallback =
                delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                { return true; };
            smtpServer.Send(mail);
            Debug.Log("success");

        }

        public string Pass(int length)
        {

            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}


