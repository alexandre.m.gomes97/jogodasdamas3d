﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

namespace JogoDasDamas
{
    public class ForgotView : MonoBehaviour
    {
        public delegate void MetodoSemParametros();
        public static event MetodoSemParametros PedidoCancel;
        public static event MetodoComDuasString PedidoRecover;

        public InputField email;
        public InputField Username;

        void Start()
        {
            ForgotModel.GoBack += ForgotModel_GoBack;
            ForgotModel.RecoverPass += ForgotModel_RecoverPass;
            ForgotModel.OnErro += ForgotModel_OnErro;
        }

        private void ForgotModel_OnErro(string msg)
        {
            MessageBox.messageInfo = msg;
            MessageBox.titleInfo = "Login Failed!";
            SceneManager.LoadSceneAsync("MessageBox", LoadSceneMode.Additive);
        }

        private void ForgotModel_RecoverPass()
        {
            MessageBox.messageInfo = "Email Sent, check email";
            MessageBox.titleInfo = "Check Email";
            SceneManager.LoadSceneAsync("MessageBox", LoadSceneMode.Additive);
        }

        private void ForgotModel_GoBack()
        {
            SceneManager.UnloadSceneAsync("ForgotPassword");
        }

        //BUTTONS START

        public void Btn_Cancel()
        {
            if (PedidoCancel != null)
                PedidoCancel();
        }

        public void Btn_Recover()
        {
            if (PedidoRecover != null)
                PedidoRecover(email.text,Username.text);
        }

        //BUTTONS END
        
        
            
        
            


        public void OnDisable()
        {
            ForgotModel.GoBack -= ForgotModel_GoBack;
            ForgotModel.RecoverPass -= ForgotModel_RecoverPass;
            ForgotModel.OnErro -= ForgotModel_OnErro;
        }
    }
}
