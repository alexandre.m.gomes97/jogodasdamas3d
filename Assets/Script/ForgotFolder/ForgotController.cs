﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;


namespace JogoDasDamas
{
    class ForgotController : MonoBehaviour
    {
        public ForgotModel ForgotModel { get; set; } = new ForgotModel();

        public void OnEnable()
        {
            ForgotView.PedidoCancel += ForgotView_PedidoCancel;
            ForgotView.PedidoRecover += ForgotView_PedidoRecover;
            ForgotModel.SendEmail += ForgotModel_SendEmail;       
        }

        private void ForgotModel_SendEmail(string email, string user, string pass)
        {
            WWWForm form = new WWWForm();
            form.AddField("UserPost", user);          
            form.AddField("passPost", pass);
            form.AddField("emailPost", email);
            WWW www = new WWW("http://localhost/Player/AlterPass.php", form);
            Thread.Sleep(500);

            
            if (www.text == "Player Not Found")
                throw new Exception("Account not Found or Email not valid");
            
        }

       

        private void ForgotView_PedidoRecover(string email, string username)
        {
            try
            {
                ForgotModel.onButtonRecover( email, username);
            }
            catch (Exception e)
            {
                ForgotModel.onError(e.Message);
            }
        }

        private void ForgotView_PedidoCancel()
        {
            ForgotModel.onButtonCancel();
        }

        public void OnDisable()
        {
            ForgotView.PedidoCancel -= ForgotView_PedidoCancel;
            ForgotView.PedidoRecover -= ForgotView_PedidoRecover;
            ForgotModel.SendEmail -= ForgotModel_SendEmail;
        }


    }
}
