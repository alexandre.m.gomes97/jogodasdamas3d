﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JogoDasDamas
{
    public class Chain
    {
        public int xStart { get; set; }
        public int yStart { get; set; }
        public int size { get; set; }

        public Chain()
        {
            size = 1;
        }

        public Chain(int _size, int x, int y)
        {
            size = _size;
            xStart = x;
            yStart = y;
        }
      
    }
}
