﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace JogoDasDamas
{
    public class Bot
    {

        public Piece SelectPiece(Piece[,] pieces)
        {
            System.Random rnd = new System.Random();
            int x = 0;
            int y = 0;
            do
            {
                do
                {
                    x = rnd.Next(0, 9); 
                    y = rnd.Next(0, 9); 
                } while (pieces[x, y] == null);

            } while (pieces[x, y].type != "Black");

            return pieces[x, y];
        }
        public Vector2 GenerateMove(bool[,]pieces)
        {
            for(int i = 0; i < 8; i++)
                for(int j = 0; j < 8; j++)
                {
                    if (pieces[i, j])
                        return new Vector2(i, j);
                }
            return new Vector2(-1, -1);
        }
    }
}
