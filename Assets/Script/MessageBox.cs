﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace JogoDasDamas
{
    public class MessageBox : MonoBehaviour
    {
        public static string messageInfo { get; set; }
        public static string titleInfo { get; set; }
        public Text Msg;
        public Text Title;

        // Start is called before the first frame update
        void Start()
        {   
            Msg.text = messageInfo;
            Title.text = titleInfo;
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void btn_Ok()
        {
            SceneManager.UnloadSceneAsync("MessageBox");
        }
    }
}

