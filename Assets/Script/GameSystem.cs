﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JogoDasDamas
{
    public class GameSystem : MonoBehaviour
    {
        public static GameSystem Instance;
        public Player PlayerLogged { get; set; }
        public Player PlayerLogged2 { get; set; }
        public Player Bot;
        public string Winner { get; set; }
        public string Loser { get; set; }
        public int PiecesRemain { get; set; }
        public string duration { get; set; }
        public bool botOn { get; set; }
        public bool ReturnGame { get; set; }
        public bool DrawOn { get; set; }
        public bool QuitGame { get; set; }

        public string guest_path { get; set; }

        public string initGame { get; set; }

        void Start()
        {
            DontDestroyOnLoad(gameObject);
            if (Instance == null)
                Instance = this;
            else if (Instance != this)
                Destroy(this);
            guest_path = Application.dataPath;
            Bot = new Player();
            Bot.username = "Computador";
            string m_Path = Application.dataPath;
            Bot.photoPath = "C:\\Users\\alex_\\Desktop\\FotosJogo\\Bot.jpg";
            //Bot.photoPath = m_Path+ "\\JogoDasDamas_Data\\Bot.jpg";
        }

    }
}
