﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace JogoDasDamas
{

    public class AskDrawFolder : MonoBehaviour
    {
        string UpdateWins = "http://localhost/Player/UpdateWins.php";
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void returnBtn()
        {
            SceneManager.UnloadSceneAsync("DrawAsk");
        }

        public void DrawAsked()
        {
            GameSystem.Instance.DrawOn = true;
            SceneManager.UnloadSceneAsync("DrawAsk");
            UpdateDatabase(GameSystem.Instance.PlayerLogged);
            UpdateDatabase(GameSystem.Instance.PlayerLogged2);
        }

        public void UpdateDatabase(Player playertoUpdate)
        {
            WWWForm form = new WWWForm();
            form.AddField("UserPost", playertoUpdate.username);
            form.AddField("winsPost", playertoUpdate.wins);
            form.AddField("drawsPost", playertoUpdate.draws);
            form.AddField("defeatsPost", playertoUpdate.defeats);
            form.AddField("ngamesPost", playertoUpdate.n_games);

            WWW www = new WWW(UpdateWins, form);
            Thread.Sleep(500);
        }
    }
}
