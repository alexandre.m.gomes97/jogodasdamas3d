﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace JogoDasDamas
{
    public class Player
    {
        public int ID_P { get; set; }
        public string name { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string email { get; set; }
        public string pais { get; set; }
        public string gender { get; set; }
        public string photoPath { get; set; }
        public string birthday { get; set; }
        public int wins { get; set; }
        public int defeats { get; set; }
        public int draws { get; set; }
        public int n_games { get; set; }
        public float WinPercentage { get; set; }
        public int a_games { get; set; }
        public string pieceType { get; set; }

        public Player(int ID_P,string name, string username, string pass, string email, string pais, string photo, string birthday, int wins, int defats, int draws, int n_game, float winP, int a_games, string pieceType)
        {
            this.ID_P = ID_P;
            this.name = name;
            this.username = username;
            this.password = pass;
            this.email = email;
            this.pais = pais;
            this.photoPath = photo;
            this.birthday = birthday;
            this.wins = wins;
            this.defeats = defeats;
            this.draws = draws;
            this.n_games = n_game;
            this.WinPercentage = winP;
            this.a_games = a_games;
            this.pieceType = pieceType;

        }

        public Player(string username, int wins, int n_games)
        {
            this.username = username;
            this.wins = wins;
            this.n_games = n_games;
            WinPercentageCalc();
        }

        public Player()
        {
            photoPath = " ";
            wins = 0;
            defeats = 0;
            draws = 0;
            n_games = 0;
            WinPercentage = 0;
            a_games = 0;
        }

        public void WinPercentageCalc()
        {
            if (n_games != 0)
                WinPercentage = ((float)wins / (float)n_games)*100;
        }

        public void ShowInfo()
        {

        }

        public void RegisterPlayer()
        {

        }

        public void ChangeInfo()
        {

        }

        public Player getCopyPlayer()
        {
            return new Player(ID_P,name, username, password, email, pais, photoPath, birthday, wins, defeats, draws, n_games, WinPercentage, a_games, pieceType);
        }
    }
}
