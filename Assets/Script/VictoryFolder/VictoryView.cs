﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


namespace JogoDasDamas
{

    public class VictoryView : MonoBehaviour
    {
        public delegate void MetodosSemParametros();
        public static event MetodosSemParametros PedidoNext;

        public Text playerName;
        public Text GameDuration;
        public Text PieceRemaning;
        public Text playerLoser;
        public RawImage Winner;
        public RawImage Loser;


        // Start is called before the first frame update
        void Start()
        {
            VictoryModel.RespostaNext += VictoryModel_RespostaNext;
            playerName.text = GameSystem.Instance.Winner;
            GameDuration.text = GameSystem.Instance.duration;
            PieceRemaning.text = GameSystem.Instance.PiecesRemain.ToString();
            playerLoser.text = GameSystem.Instance.Loser;
            
        if(GameSystem.Instance.PlayerLogged2 !=null) { 
            if((GameSystem.Instance.Winner == GameSystem.Instance.PlayerLogged.username))
             {
                WWW newImage = new WWW("file:///" + GameSystem.Instance.PlayerLogged.photoPath);
                Winner.texture = newImage.texture;
                WWW newImage2 = new WWW("file:///" + GameSystem.Instance.PlayerLogged2.photoPath);
                Loser.texture = newImage2.texture;
            }
            else if(GameSystem.Instance.Winner == GameSystem.Instance.PlayerLogged2.username)
            {
                WWW newImage = new WWW("file:///" + GameSystem.Instance.PlayerLogged2.photoPath);
                Winner.texture = newImage.texture;
                WWW newImage2 = new WWW("file:///" + GameSystem.Instance.PlayerLogged.photoPath);
                Loser.texture = newImage2.texture;
            }
          }
            else if(GameSystem.Instance.Winner == GameSystem.Instance.PlayerLogged.username)
            {
                WWW newImage = new WWW("file:///" + GameSystem.Instance.PlayerLogged.photoPath);
                Winner.texture = newImage.texture;
                WWW newImage2 = new WWW("file:///" + GameSystem.Instance.Bot.photoPath);
                Loser.texture = newImage2.texture;
            }
            else
            {
                WWW newImage = new WWW("file:///" + GameSystem.Instance.Bot.photoPath);
                Winner.texture = newImage.texture;
                WWW newImage2 = new WWW("file:///" + GameSystem.Instance.PlayerLogged.photoPath);
                Loser.texture = newImage2.texture;
            }

        }

        private void VictoryModel_RespostaNext()
        {
            SceneManager.LoadScene("Menu");
        }

        public void Btn_Next()
        {
            if (PedidoNext != null)
                PedidoNext();
        }


        public void OnDisable()
        {
            VictoryModel.RespostaNext -= VictoryModel_RespostaNext;
        }
    }
}
