﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;


namespace JogoDasDamas
{

    public class VictoryController : MonoBehaviour
    {
        public VictoryModel VictoryModel { get; set; } = new VictoryModel();
        string AddNewGame = "http://localhost/Game/GameInserter.php";
        string UpdateWins = "http://localhost/Player/UpdateWins.php";
        public void OnEnable()
        {
            VictoryView.PedidoNext += VictoryView_PedidoNext;
            VictoryModel.UpdateData += VictoryModel_UpdateData;
        }

        private void VictoryModel_UpdateData()
        {

            if (GameSystem.Instance.Winner == GameSystem.Instance.PlayerLogged.username)
            {
                GameSystem.Instance.PlayerLogged.wins++;
                GameSystem.Instance.PlayerLogged.n_games++;
                UpdateDatabase(GameSystem.Instance.PlayerLogged);
            }
            if (GameSystem.Instance.Loser == GameSystem.Instance.PlayerLogged.username)
            {
                GameSystem.Instance.PlayerLogged.defeats++;
                GameSystem.Instance.PlayerLogged.n_games++;
                UpdateDatabase(GameSystem.Instance.PlayerLogged);
            }



            if (GameSystem.Instance.PlayerLogged2 != null)
            {

                if (GameSystem.Instance.Winner == GameSystem.Instance.PlayerLogged2.username)
                {
                    GameSystem.Instance.PlayerLogged2.wins++;
                    GameSystem.Instance.PlayerLogged2.n_games++;
                    UpdateDatabase(GameSystem.Instance.PlayerLogged2);
                }
                if (GameSystem.Instance.Loser == GameSystem.Instance.PlayerLogged2.username)
                {
                    GameSystem.Instance.PlayerLogged2.defeats++;
                    GameSystem.Instance.PlayerLogged2.n_games++;
                    UpdateDatabase(GameSystem.Instance.PlayerLogged2);
                }
            }





            if (GameSystem.Instance.PlayerLogged2 != null && GameSystem.Instance.PlayerLogged2.username != "Guest")
            {
                InsertGameTwoPlayers();
            }
            else
            {
                InsertGame();
            }




        }

        public void InsertGameTwoPlayers()
        {
            WWWForm form = new WWWForm();
            form.AddField("ID_P1Post", GameSystem.Instance.PlayerLogged.ID_P);
            form.AddField("ID_P2Post", GameSystem.Instance.PlayerLogged2.ID_P);
            form.AddField("StartGamePost", GameSystem.Instance.initGame);
            form.AddField("DurGamePost", GameSystem.Instance.duration);

            if (GameSystem.Instance.Winner == GameSystem.Instance.PlayerLogged.username)
            {
                form.AddField("WinnerPost", GameSystem.Instance.PlayerLogged.username);
                form.AddField("LoserPost", GameSystem.Instance.PlayerLogged.username);
            }

            if (GameSystem.Instance.Winner == GameSystem.Instance.PlayerLogged2.username)
            {
                form.AddField("WinnerPost", GameSystem.Instance.PlayerLogged2.username);
                form.AddField("LoserPost", GameSystem.Instance.PlayerLogged2.username);
            }


            WWW www = new WWW(AddNewGame, form);
            Thread.Sleep(500);
        }

        public void InsertGame()
        {
            WWWForm form = new WWWForm();
            form.AddField("ID_P1Post", GameSystem.Instance.PlayerLogged.ID_P);
            if (GameSystem.Instance.PlayerLogged2 != null)
                form.AddField("ID_P2Post", "2");
            else
                form.AddField("ID_P2Post", "1");
            form.AddField("StartGamePost", GameSystem.Instance.initGame);
            form.AddField("DurGamePost", GameSystem.Instance.duration);

            if (GameSystem.Instance.Winner == GameSystem.Instance.PlayerLogged.username)
            {
                form.AddField("WinnerPost", GameSystem.Instance.PlayerLogged.username);
                if (GameSystem.Instance.PlayerLogged2 != null)
                    form.AddField("LoserPost", GameSystem.Instance.PlayerLogged2.username);
                else
                    form.AddField("LoserPost", "Bot");
            }

            else
            {
                if (GameSystem.Instance.PlayerLogged2 != null)
                {
                    form.AddField("WinnerPost", GameSystem.Instance.PlayerLogged2.username);
                    form.AddField("LoserPost", GameSystem.Instance.PlayerLogged.username);
                }
                else
                {
                    form.AddField("WinnerPost", "Bot");
                    form.AddField("LoserPost", GameSystem.Instance.PlayerLogged.username);
                }
            }




            WWW www = new WWW(AddNewGame, form);
            Thread.Sleep(500);
        }
        public void UpdateDatabase(Player playertoUpdate)
        {
            WWWForm form = new WWWForm();
            form.AddField("UserPost", playertoUpdate.username);
            form.AddField("winsPost", playertoUpdate.wins);
            form.AddField("drawsPost", playertoUpdate.draws);
            form.AddField("defeatsPost", playertoUpdate.defeats);
            form.AddField("ngamesPost", playertoUpdate.n_games);

            WWW www = new WWW(UpdateWins, form);
            Thread.Sleep(500);
        }

        string GetDataValue(string data, string index)
        {
            string value = data.Substring(data.IndexOf(index) + index.Length);
            if (value.Contains("|"))
                value = value.Remove(value.IndexOf('|'));
            return value;
        }

        private void VictoryView_PedidoNext()
        {
            VictoryModel.onButtonNext();
        }

        public void OnDisable()
        {
            VictoryView.PedidoNext -= VictoryView_PedidoNext;
            VictoryModel.UpdateData -= VictoryModel_UpdateData;
        }
    }
}
