﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace JogoDasDamas
{

    public class VictoryModel
    {
        public delegate void MetodosSemParametros();
        public static event MetodosSemParametros RespostaNext;
        public static event MetodoSemParametros UpdateData;
        public void onButtonNext()
        {
            if (UpdateData != null)
                UpdateData();
            if (RespostaNext != null)
                RespostaNext();
        }

    }
}
