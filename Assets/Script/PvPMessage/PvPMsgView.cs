﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace JogoDasDamas
{

    public class PvPMsgView : MonoBehaviour
    {
        public static event MetodoSemParametros PedidoCancel;
        public static event MetodoSemParametros PedidoLogin;
        public static event MetodoSemParametros PedidoGuest;


        public static bool loginSecond = false;

        

        void Start()
        {
            PvPMsgModel.Cancel += PvPMsgModel_Cancel;
            PvPMsgModel.RespostaLogin += PvPMsgModel_RespostaLogin;
            PvPMsgModel.RespostaGuest += PvPMsgModel_RespostaGuest;
        }

        private void PvPMsgModel_RespostaGuest()
        {
            GameSystem.Instance.botOn = false;
            SceneManager.LoadScene("GameZone", LoadSceneMode.Single);
            
        }

        private void PvPMsgModel_RespostaLogin()
        {
            loginSecond = true;
            GameSystem.Instance.botOn = false;
            SceneManager.UnloadSceneAsync("PvPMessage");
            SceneManager.LoadScene("Login", LoadSceneMode.Additive);
        }

        private void PvPMsgModel_Cancel()
        {
            SceneManager.UnloadSceneAsync("PvPMessage");
        }

        public void CancelBtn()
        {
            if (PedidoCancel != null)
                PedidoCancel();
        }

        public void Btn_Login()
        {
            if (PedidoLogin != null)
                PedidoLogin();
        }

        public void Btn_Guest()
        {
            if (PedidoGuest != null)
                PedidoGuest();
        }



        // Update is called once per frame
        void Update()
        {

        }

        public void OnDisable()
        {
            PvPMsgModel.Cancel -= PvPMsgModel_Cancel;
            PvPMsgModel.RespostaLogin -= PvPMsgModel_RespostaLogin;
            PvPMsgModel.RespostaGuest -= PvPMsgModel_RespostaGuest;
        }

    }
}
    