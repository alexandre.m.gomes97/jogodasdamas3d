﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

namespace JogoDasDamas
{
    public class PvPMsgController : MonoBehaviour
    {
        public PvPMsgModel PvPMsgModel { get; set; } = new PvPMsgModel();

        public void OnEnable()
        {
            PvPMsgView.PedidoCancel += PvPMsgView_PedidoCancel;
            PvPMsgView.PedidoLogin += PvPMsgView_PedidoLogin;
            PvPMsgView.PedidoGuest += PvPMsgView_PedidoGuest;
        }

        private void PvPMsgView_PedidoGuest()
        {
            PvPMsgModel.OnGuestButton();
        }

        private void PvPMsgView_PedidoLogin()
        {
            PvPMsgModel.OnLoginButton();
        }

        private void PvPMsgView_PedidoCancel()
        {
            PvPMsgModel.OnCancelButton();
        }

        public void OnDisable()
        {
            PvPMsgView.PedidoCancel -= PvPMsgView_PedidoCancel;
            PvPMsgView.PedidoLogin -= PvPMsgView_PedidoLogin;
            PvPMsgView.PedidoGuest -= PvPMsgView_PedidoGuest;
        }

    }
}



