﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace JogoDasDamas
{
    public class PvPMsgModel
    {
        
        public static event MetodoSemParametros Cancel;
        public static event MetodoSemParametros RespostaLogin;
        public static event MetodoSemParametros RespostaGuest;
        private Player Guest;

        public PvPMsgModel()
        {

            //string photoPath = GameSystem.Instance.guest_path + "\\JogoDasDamas_Data\\Guest.jpg";
            string photoPath = "C:\\Users\\alex_\\Desktop\\FotosJogo\\Guest.jpg";

            Guest = new Player(0,"Guest","Guest", "Guest", "Guest", "Guest", photoPath, "Guest",0,0,0,0,0,0, "Guest");
        }

        public void OnCancelButton()
        {
            if (Cancel != null)
                Cancel();
        }

        public void OnLoginButton()
        {     
               if(RespostaLogin!=null)
                RespostaLogin();           
        }
       
        public void OnGuestButton()
        {
            if (RespostaGuest != null)
            {
                RespostaGuest();
                InitGuest();
                GameSystem.Instance.PlayerLogged2 = Guest;
            }
                
        }

        public void InitGuest()
        {
            Guest.name = "Guest";
            Guest.username = "Guest";
            Guest.pais = "Unknown";
            Guest.pieceType = "Black";
            Guest.gender = "Unknown";
            Guest.email = "guest@checkersgame.pt";
        }

    }
}
