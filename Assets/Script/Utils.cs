﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace JogoDasDamas
{
    public delegate void MetodoSemParametros();
    public delegate void MetodoCheckValidMove(bool queen);
    public delegate void MetodoMovePiece(int xE, int yE);
    public delegate void MetodoComUmaString(string msg);
    public delegate void MetodoComUmBool(bool Second);
    public delegate void MetodoComDuasString(string user, string pass);
    public delegate void MetodoComTresString(string user, string email, string pass);
    public delegate void MetodoComUmPlayer(Player player);



    public class Utils
    {
        public static Player Player1 { get; private set; } = new Player();

        public Vector3 boardOffset = new Vector3(-4.0f, 0, -4.0f);
        public Vector3 pieceOffSet = new Vector3(0.501f, 0, 0.55f);
        public  float xBlackOffSet = (float)0.75;
        public  float yBlackOffSet = (float)-2.5;


        public  float xWhiteOffSet = (float)0.75;
        public  float yWhiteOffSet = (float)9.5;

        

    }


}
